# Voxel Game

This is a minecraft style voxel game created by Heath Sargent as a project for university.
More information can be found at: https://heathsargent.wixsite.com/games/voxel-game

Compile:
glfw3.lib needs to be placed in Dep\Lib\
Raknet libraries need to be placed in Dep\Raknet\Lib
(Even though networking was never complete, the Raknet library dependency and networking code was never removed)