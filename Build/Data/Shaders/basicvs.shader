#version 410

layout(location = 0) in vec4 Position;
layout(location = 1) in vec4 Normal;
layout(location = 2) in vec4 Tangent;
layout(location = 3) in vec4 Colour;
layout(location = 4) in vec2 Uv;

out vec4 vPos;
out vec4 vColour;

uniform mat4 projectionViewWorldMatrix;
uniform mat4 modelMatrix;

void main()
{
	vPos = modelMatrix * Position;
	vColour = Colour;
	gl_Position = projectionViewWorldMatrix * vPos;
}