#version 410

out vec4 fragColor;
in vec4 vPos;
in vec4 vNormal;
in vec4 vCol;
in float vLight;
in vec2 vUv;
in float vAO;
in float vGI;
in vec3 vAOColour;

uniform int UseTexture;

uniform float LinearGI;
uniform float ExponentialGI;

uniform sampler2D atlasTex;
uniform vec4 LightPos;

void main() 
{
	if(UseTexture == 1 && texture(atlasTex, vUv).a <= 0.2f)
		discard;

    vec3 aCol = vCol.rgb;
       
    float rvGI = (vGI - 0.5f);
    rvGI = rvGI - (rvGI - 0.5f) * LinearGI;
       
    if(rvGI > 0.0f)
        aCol = (1.0f - rvGI) * aCol + vAOColour * rvGI;
        
	vec3 lightDir = vPos.xyz - LightPos.xyz;
	float lightAmount = max(0, dot(-normalize(lightDir), vNormal.xyz));
	
    float dist = length(lightDir);
    
    float d = (dot(vNormal.xyz, -(normalize(vec3(0.5,-1.5,0.8)))) + 1.0f) / 2.0f;

    float dStr = 0.3f;
    float aStr = 0.5f;
    //float pStr = lightAmount / 5.0f * (1.0f / (pow(dist, 1.5f))) * 10.0f;
    //if(pStr > 0.4f)
    //    pStr = 0.4f;
    //vec3 pCol = vec3(1,0.6f,0);
    
    if(UseTexture == 1)
        fragColor = vec4(aCol * texture(atlasTex, vUv).rgb * (aStr + dStr * d), vCol.a) ;
    else
        fragColor = vec4(aCol * (aStr + dStr * d), vCol.a) ;
    
    //Used to be 16, now 100 chunks, pretty much disabled.
    float fadeStart = 100.0f * 16.0f;
    float fadeAmt = 0.0f;
    
    if(dist >= fadeStart)
        fadeAmt = (dist - fadeStart) / (16.0f * 2.0f);
        
    if(fadeAmt > 1.0f)
        fadeAmt = 1.0f;
       
    float rVAO = vAO - 0.25f;
       
    if(rVAO > 0.0f)
        fragColor.rgb = fragColor.rgb * (1.0f - rVAO);
        
    fragColor = vec4((1.0f - fadeAmt) * fragColor.rgb, fragColor.a) + vec4(fadeAmt * vec3(0.7f, 0.7f, 1.0f), fragColor.a);
}