#version 410

out vec4 fragColor;
in vec4 vPos;
in vec4 vColour;

void main() 
{
	fragColor = vColour;
}