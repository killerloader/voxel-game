#version 410

layout(location=0) in vec4 position;
layout(location=1) in vec4 normal;
layout(location=2) in vec4 colour;
layout(location=3) in vec4 AOcolour;

out vec4 vPos;
out vec4 vNormal;
out float vLight;
out vec4 vCol;
out vec3 vAOColour;
out vec2 vUv;
out float vAO;
out float vGI;

uniform mat4 projectionViewWorldMatrix;

uniform vec3 inPos;

void main()
{
	vNormal = vec4(normal.xyz, 0);
	vPos = vec4(position.xyz + inPos, 1);
	vLight = max(0, dot(normal, normalize(vec4(1,0.5,0.3,1))));
	vUv = vec2(position.w, normal.w);
	vCol = colour;
    vAO = position.w;
    vGI = AOcolour.w;
    vAOColour = AOcolour.rgb;
	gl_Position = projectionViewWorldMatrix * vPos;
}