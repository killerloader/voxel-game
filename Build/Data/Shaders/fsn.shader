#version 410

out vec4 fragColor;
in vec4 vPos;
in vec4 vNormal;
in vec4 vCol;
in float vLight;
in vec2 vUv;

uniform int UseTexture;

uniform sampler2D atlasTex;
uniform vec4 LightPos;

void main() 
{
	if(texture(atlasTex, vUv).a <= 0.2f && UseTexture == 1)
		discard;

	vec3 lightDir = vPos.xyz - LightPos.xyz;
	float lightAmount = max(0, dot(-normalize(lightDir), vNormal.xyz));
	
    float dist = length(lightDir);
    
	float d = max(0, dot(vNormal.xyz, -(normalize(vec3(0.3,-1,0.5)))));

    float dStr = 0.3f;
    float aStr = 0.7f;
    float pStr = lightAmount / 5.0f * (1.0f / (pow(dist, 1.5f))) * 10.0f;
    if(pStr > 0.4f)
        pStr = 0.4f;
    vec3 pCol = vec3(1,0.6f,0);
    
    vec3 eCol = vec3(1,1,1);
    
    if(UseTexture == 1)
        eCol = texture(atlasTex, vUv).rgb;
    
	fragColor = vec4(vCol.rgb * eCol * (aStr + dStr * d) + pStr * pCol, vCol.a) ;
    }