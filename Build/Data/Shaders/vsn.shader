#version 410

layout(location=0) in vec4 position;
layout(location=1) in vec4 normal;
layout(location=2) in vec4 tangent;
layout(location=3) in vec4 colour;
layout(location=4) in vec2 uv;

out vec4 vPos;
out vec4 vNormal;
out float vLight;
out vec4 vCol;
out vec2 vUv;

uniform mat4 projectionViewWorldMatrix;

uniform vec3 inPos;

void main()
{
	vNormal = normal;
	vPos = vec4(position.xyz + inPos, position.w);
	vLight = max(0, dot(normal, normalize(vec4(1,0.5,0.3,1))));
	vUv = uv;
	vCol = colour;
	gl_Position = projectionViewWorldMatrix * vPos;
}