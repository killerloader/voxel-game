#pragma once
#include "BaseMap.h"

class PlayerCamera;
class Mesh;
class Shader;
class Landscape;
class Player;
class Client;

//Minecraft map. Landscape, collision and player movement.
class MinecraftMap : public BaseMap
{
public:
	MinecraftMap();	//Constructor.	
	~MinecraftMap();//Destructor.

	void Draw();			//Draw method. Mainly branches off to landscape to draw that.
	void Update(float dt);	//Update map.
	void DrawUI();			//Draw UI.

private:
	PlayerCamera* m_camera;	//Camera.
	Shader* m_shader;	//Main minecraft shader.
	Shader* m_shaderNormal;	//Main minecraft shader.

	Landscape* m_landscape;	//landscape pointer.
	Player* m_player;	//Player pointer.

	Client* m_client;	//Client for online play.
};