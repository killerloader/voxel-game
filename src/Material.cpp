#include "Material.h"


//Constructor. Set default material values.
Material::Material()
{
	m_normlMapEnabled = true;
	m_specular = 1.0f;
	m_roughness = 0.0f;
	m_metalic = 0.0f;
	m_diffuseTexture = nullptr;
	m_normalMapTexture = nullptr;
	m_specularMapTexture = nullptr;
}

Material::~Material()
{
}

//Set the normal map texture.
void Material::SetNormalMap(Texture * texture)
{
	m_normalMapTexture = texture;
}

//Set the diffuse texture for the material.
void Material::SetDiffuse(Texture * texture)
{
	m_diffuseTexture = texture;
}

//Set the specular map for the material.
void Material::SetSpecularMap(Texture * texture)
{
	m_specularMapTexture = texture;
}

//Set the material roughness.
void Material::SetRoughness(float roughness)
{
	m_roughness = roughness;
}

//Set the material specular.
void Material::SetSpecular(float specular)
{
	m_specular = specular;
}

//Set whether the normal map should be used or not (for toggle)
void Material::SetNormalMapEnabled(bool enabled)
{
	m_normlMapEnabled = enabled;
}

//Check whether the normal map is enabled.
bool Material::GetNormalMapEnabled()
{
	return m_normlMapEnabled;
}

//Get the diffuse texture. (can be nullptr)
Texture * Material::GetDiffuse()
{
	return m_diffuseTexture;
}

//Get the normal map texture. (can be nullptr)
Texture * Material::GetNormalMap()
{
	return m_normalMapTexture;
}

//Get the specular map texture. (can be nullptr)
Texture * Material::GetSpecularMap()
{
	return m_specularMapTexture;
}

//Get the roughness of the material.
float Material::GetRoughness()
{
	return m_roughness;
}

//Get the specularity of the material.
float Material::GetSpecular()
{
	return m_specular;
}

//Get the metalic value of the material.
float Material::GetMetalic()
{
	return m_metalic;
}
