#include "Shader.h"
#include "Texture.h"
#include <fstream>
#include "ResourceManager.h"
#include <iostream>

int Shader::m_currentBoundProgramID = 0;

Shader* Shader::m_currentBoundProgram = nullptr;

//Returns a point to the currently bound shader.
Shader * Shader::GetCurrentProgramShader()
{
	return m_currentBoundProgram;
}

//Check if any program is currently bound, this will return false if no shader is bound.
bool Shader::IsAnyProgramBound()
{
	return (m_currentBoundProgram != 0);
}

//Bind a program id.
void Shader::BindProgramID(int programID)
{
	m_currentBoundProgramID = programID;
	glUseProgram(programID);
}

//Load from a file and place in a string.
std::string Shader::LoadFromFile(std::string fileName)
{
	std::fstream shaderFile(fileName, std::ios::in);

	if (!shaderFile.is_open())
	{
		std::cout << "Failed to open shader file!" << std::endl;
		return "";
	}

	std::string line;
	std::string strWhole;

	while (std::getline(shaderFile, line))
	{
		if (line.empty())
			continue;

		strWhole += line + '\n';
	}

	return strWhole;
}

//Bind this shader.
void Shader::Bind()
{
	if (m_programID == 0)
		return;

	BindProgramID(m_programID);
	m_currentBoundProgram = this;
}

//Unbind any shaders currently bound.
void Shader::UnbindAny()
{
	BindProgramID(0);
	m_currentBoundProgram = nullptr;
}

//Compare this shader to another using their program ID.
bool Shader::operator==(const Shader other)
{
	return (m_programID == other.m_programID);
}

//Compare this shader to another use thier shader ID.
bool Shader::operator==(const EShaderID otherID)
{
	return (otherID == m_shaderID);
}

//Get the ID of this shader's program.
int Shader::GetProgramID()
{
	return m_programID;
}

//Get this shader's shader ID.
EShaderID Shader::GetShaderID()
{
	return m_shaderID;
}

//Constructor. (unused, as the shader gets created via static methods)
Shader::Shader()
{
}

//Destructor.
Shader::~Shader()
{
	if (m_currentBoundProgram == this)
		UnbindAny();

	if(m_programID != 0)
		glDeleteProgram(m_programID);
}

//Create a shdaer based on two file names.
Shader * Shader::CreateShader(std::string shaderName, EShaderID shaderID, std::string vertexFileName, std::string fragmentFileName)
{
	Shader* newShader = new Shader;

	newShader->m_shaderID = shaderID;

	newShader->m_programID = 0;

	//Load files with file names and store in a string.
	std::string vertexSource = LoadFromFile(vertexFileName);
	std::string fragmentSource = LoadFromFile(fragmentFileName);

	//Convert to const char*
	const char* vsSrc = vertexSource.c_str();
	const char* fsSrc = fragmentSource.c_str();

	//Create shader program.
	
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(vertexShader, 1, &vsSrc, 0);
	glCompileShader(vertexShader);
	glShaderSource(fragmentShader, 1, &fsSrc, 0);
	glCompileShader(fragmentShader);
	newShader->m_programID = glCreateProgram();
	glAttachShader(newShader->m_programID, vertexShader);
	glAttachShader(newShader->m_programID, fragmentShader);
	glLinkProgram(newShader->m_programID);
	int success = GL_FALSE;
	glGetProgramiv(newShader->m_programID, GL_LINK_STATUS, &success);
	if (success == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetProgramiv(newShader->m_programID, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];
		glGetProgramInfoLog(newShader->m_programID, infoLogLength, 0, infoLog);
		printf("Error: Failed to link shader program!\n");
		printf("%s\n", infoLog); delete[] infoLog;
	}
	glDeleteShader(fragmentShader);
	glDeleteShader(vertexShader);

	//Add shader to resource manager if possible. Otherwise destroy shader.
	if (!ResourceManager::Instance().AddShader(newShader, shaderName))
	{
		delete newShader;
		return nullptr;
	}

	return newShader;
}

//Create a shader program, ignore shader ID (will be set to basic, which is the default)
Shader * Shader::CreateShader(std::string shaderName, std::string vertexFileName, std::string fragmentFileName)
{
	return CreateShader(shaderName, EShaderID_Basic, vertexFileName, fragmentFileName);
}

//Check if this program is bound.
bool Shader::IsProgramBound()
{
	return (m_currentBoundProgramID == m_programID && m_currentBoundProgramID != 0);
}

//Find a variable in this shader.
//Stores a map of variables for optimization, so will never have to use glGetUniformLocation twice for the same variable.
int Shader::FindVariable(std::string variableName)
{
	//Variable doesn't exist, create it
	if (m_programVariables.count(variableName) == 0)
	{
		int newID = -1;

		newID = glGetUniformLocation(m_programID, variableName.c_str());
		m_programVariables.insert(std::pair<std::string, int>(variableName, newID));

		return newID;
	}

	return m_programVariables[variableName];
}

//Set a shader uniform int.
void Shader::SetInt(std::string variableName, int value)
{
	int varID = FindVariable(variableName);

	if (varID == -1)
		return;

	glUniform1i(varID, value);
}

//Set a shader uniform texture given a texture slot, texture and uniform name.
void Shader::SetTexture(std::string variableName, Texture* texture, int slotID)
{
	int loc = FindVariable(variableName);

	//Fail if location doesn't exist, or texture doesn't exist.
	if (loc == -1 || texture->GetTextureID() == -1)
		return;

	// set texture slot 
	// Because slot ID does not start at "0", we have to get the actual ID of the first slot, than add the slot number onto it.
	glActiveTexture(GL_TEXTURE0 + slotID);
	glBindTexture(GL_TEXTURE_2D, texture->GetTextureID());

	// tell the shader where it is 
	glUniform1i(loc, slotID);
}

//Set a shader uniform float.
void Shader::SetFloat(std::string variableName, float value)
{
	int varID = FindVariable(variableName);

	if (varID == -1)
		return;

	glUniform1f(varID, value);
}

//Set a shader uniform vector 4
void Shader::SetVec4(std::string variableName, const vec4& value)
{
	int varID = FindVariable(variableName);

	if (varID == -1)
		return;

	glUniform4f(varID, value.x, value.y, value.z, value.a);
}

//Set a shader uniform vector 3
void Shader::SetVec3(std::string variableName, const vec3& value)
{
	int varID = FindVariable(variableName);

	if (varID == -1)
		return;

	glUniform3f(varID, value.x, value.y, value.z);
}

//Set a shader uniform vector 2
void Shader::SetVec2(std::string variableName, const vec2& value)
{
	int varID = FindVariable(variableName);

	if (varID == -1)
		return;

	glUniform2f(varID, value.x, value.y);
}

//Set a shader uniform matrix 4x4
void Shader::SetMat4(std::string variableName, const mat4& value)
{
	int varID = FindVariable(variableName);

	if (varID == -1)
		return;

	glUniformMatrix4fv(varID, 1, GL_FALSE, &(value[0][0]));
}