#include "Player.h"
#include "Landscape.h"
#include "Application.h"
#include "Shader.h"
#include "PlayerCamera.h"
#include "CollisionRay.h"
#include "LandscapeChunk.h"
#include <iostream>

//Player constructor. sets up transform and collider data.
Player::Player(BaseCamera& camera) : m_boxCollider(vec3(0.8,1.8, 0.8)), m_camera(camera)
{
	m_transform.SetPosition(vec3(10, 255, 10));
	m_boxCollider.SetTranslation(vec3(m_transform.GetWorldTransform()[3]));

	m_onGround = false;
	m_velocity = vec3(0);
}

//Destructor
Player::~Player()
{
}

//Sets the internal landscape ptr for collision purposes.
void Player::SetLandscapeInstance(Landscape * lndscpPtr)
{
	m_landscapePtr = lndscpPtr;
}

//Update method.
//Handles collision, movement, and world interaction.
void Player::Update(float dt)
{
	//return;

	UpdateBlockPlacement();

	float spdMult = 1.0f;
	bool speedHack = false;

	if (glfwGetKey(Application::Instance().GetWindow(), GLFW_KEY_E))
	{
		spdMult = 20.0f;
		speedHack = true;
	}

	//No ground below.
	if (m_onGround && !m_landscapePtr->CheckCollisionOffset(m_boxCollider, vec3(0, -0.1f, 0)))
		m_onGround = false;

	vec2 moveDir(0);

	//Fall if not on ground.
	if (!m_onGround)
		m_velocity.y -= 100.0f * dt;

	if ((m_onGround || speedHack) && glfwGetKey(Application::Instance().GetWindow(), GLFW_KEY_SPACE))
		m_velocity.y = 20.0f;

	//Get the player's grid location, used for placing and destroy blocks around player.
	vec3 blockPosition;
	blockPosition.x = glm::round(m_transform.GetWorldTransform()[3].x);
	blockPosition.y = glm::round(m_transform.GetWorldTransform()[3].y);
	blockPosition.z = glm::round(m_transform.GetWorldTransform()[3].z);

	//If Z is pressed, destroy blocks in cube area.
	if (glfwGetKey(Application::Instance().GetWindow(), GLFW_KEY_Z))
	{
		int extents = 2;

		//Destoy blocks in 3 dimensions.
		for (int i = -extents; i <= extents; i++)
			for (int ii = -extents; ii <= extents; ii++)
				for (int iii = -extents; iii <= extents; iii++)
					m_landscapePtr->DestroyBlock((int)(blockPosition.x + i), (int)(blockPosition.y + ii), (int)(blockPosition.z + iii));
	}

	//Place a block is X is press.
	if (glfwGetKey(Application::Instance().GetWindow(), GLFW_KEY_X))
	{
		m_landscapePtr->CreateBlock((int)(blockPosition.x), (int)(blockPosition.y - 1), (int)(blockPosition.z), rand() % 5 + 1);
	}

	//Get camera forward and right vector. Forward is inverted because camera's face the opposite way of their transform.
	vec3 forward = -m_camera.GetTransform().Forward();
	vec3 right = m_camera.GetTransform().Right();

	//Handle movement (WADS)
	//move along camera axis.
	if (glfwGetKey(Application::Instance().GetWindow(), GLFW_KEY_W))
		moveDir += vec2(forward.x, forward.z);

	if (glfwGetKey(Application::Instance().GetWindow(), GLFW_KEY_S))
		moveDir -= vec2(forward.x, forward.z);

	if (glfwGetKey(Application::Instance().GetWindow(), GLFW_KEY_A))
		moveDir -= vec2(right.x, right.z);

	if (glfwGetKey(Application::Instance().GetWindow(), GLFW_KEY_D))
		moveDir += vec2(right.x, right.z);

	//Check if we're moving at all.
	if (moveDir != vec2(0,0))
	{
		//Calculate acceleration and outcome speed.
		m_velocity += vec3(glm::normalize(moveDir).x,0, glm::normalize(moveDir).y) * 70.0f * spdMult * dt;

		//Limit out speed to 10.
		if (glm::length(vec2(m_velocity.x, m_velocity.z)) > 10.0f * spdMult)
		{
			vec2 newMove = glm::normalize(vec2(m_velocity.x, m_velocity.z)) * 10.0f * spdMult;
			m_velocity = vec3(newMove.x, m_velocity.y, newMove.y);
		}
	}
	else if(vec2(m_velocity.x, m_velocity.z) != vec2(0))//If we're no accelerating, but are still moving, slow down.
	{
		//Calculate deacceleration.
		vec2 slowDown = glm::normalize(vec2(m_velocity.x, m_velocity.z)) * 70.0f * dt;

		//Slow down by deacceleration.
		m_velocity.x -= slowDown.x;
		m_velocity.z -= slowDown.y;

		//If we're close enough to stopping, completely stop.
		if (glm::length(vec2(m_velocity.x, m_velocity.z)) <= 70.0f * dt)
		{
			m_velocity.x = 0.0f;
			m_velocity.z = 0.0f;
		}
	}

	//Calculate movement vector for this frame.
	vec3 thisMove = m_velocity * dt;

	//Translate object and update box colllider.
	m_transform.Translate(thisMove);
	m_boxCollider.SetTranslation(vec3(m_transform.GetWorldTransform()[3]));

	//Loop 10 times, to ensure out of object.
	if(!speedHack)
	for (int i = 0; i < 10; i++)
	{
		vec3 test(0);

		//If no collision, exit out of checks.
		if (!m_landscapePtr->CheckCollision(m_boxCollider, &test))
			break;

		//Collision was found, but no overlap was reported, assume stuck in ground.
		if (test == vec3(0))
		{
			//First translate back to previous position
			m_transform.Translate(-thisMove);
			m_boxCollider.SetTranslation(vec3(m_transform.GetWorldTransform()[3]));

			//if there is a collision at this point, then it may not be possible to get unstuck with this method.
			if (m_landscapePtr->CheckCollision(m_boxCollider, &test))
				break;

			float distCheck = glm::length(thisMove) / 2.0f;
			vec3 direction = glm::normalize(thisMove);
			//Push player to the object, have the distance at a time.
			for (int p = 0; p < 10; p++)
			{
				if (!m_landscapePtr->CheckCollisionOffset(m_boxCollider, direction * distCheck, &test))
				{
					//Translate to new position.
					m_transform.Translate(direction * distCheck);
					m_boxCollider.SetTranslation(vec3(m_transform.GetWorldTransform()[3]));
				}

				//Update distance check for next loop, so it checks half the distance.
				distCheck /= 2.0f;
			}

			m_velocity.y = 0.0f;
		
			break;
		}

		//Normalized direction of separation.
		//This is pretty much the normal of the cube on impact.
		vec3 norm = glm::normalize(test);

		//Check if no block in normalized direction.
		m_transform.Translate(test);

		//Update collider.
		m_boxCollider.SetTranslation(vec3(m_transform.GetWorldTransform()[3]));

		if (test.y != 0)
			m_onGround = true;

		//Only do if has velocity, or will create NAN problem.
		if (m_velocity != vec3(0))
		{
			//Reaction Force
			float amt = glm::dot(glm::normalize(m_velocity), -norm);
			m_velocity += norm * glm::length(m_velocity) * glm::max(0.0f, amt);
		}
	}

}

bool intersection = false;
vec3 newBlockSlot;
vec3 intersectionPoint;
vec3 faceCenter;

//Esentially to split this part of the update into a separate method.
void Player::UpdateBlockPlacement()
{
	bool pressNow = glfwGetMouseButton(Application::Instance().GetWindow(), GLFW_MOUSE_BUTTON_LEFT);

	static bool lmbPressed = false;
	static bool lmbReleased = false;
	static bool lmbDown = false;

	lmbPressed = !lmbDown && pressNow;
	lmbReleased = lmbDown && !pressNow;
	lmbDown = pressNow;

	bool pressNowR = glfwGetMouseButton(Application::Instance().GetWindow(), GLFW_MOUSE_BUTTON_RIGHT);

	static bool rmbPressed = false;
	static bool rmbReleased = false;
	static bool rmbDown = false;

	rmbPressed = !rmbDown && pressNowR;
	rmbReleased = rmbDown && !pressNowR;
	rmbDown = pressNowR;

	vec3 dir = -m_camera.GetTransform().Forward();

	CollisionRay cameraRay(m_camera.GetTransform().GetPosition(), glm::normalize(dir));

	vec3 blockLoc;

	intersection = m_landscapePtr->CheckCollision(cameraRay, 8.0f, &intersectionPoint, &blockLoc);

	//No need to draw anything at the moment.
	if (intersection)
	{
		if (rmbPressed)
		{
			m_landscapePtr->DestroyBlock(blockLoc.x, blockLoc.y, blockLoc.z);
		}

		//Calculate direction of block.

		vec3 diffLoc = intersectionPoint - blockLoc;
		vec3 offset;

		float curHighest = 0.0f;

		//Find which direction is the most viable direction.
		if (abs(diffLoc.x) > curHighest)
		{
			curHighest = abs(diffLoc.x);
			offset = vec3(glm::sign(diffLoc.x), 0, 0);
		}

		if (abs(diffLoc.y) > curHighest)
		{
			curHighest = abs(diffLoc.y);
			offset = vec3(0, glm::sign(diffLoc.y), 0);
		}

		if (abs(diffLoc.z) > curHighest)
		{
			curHighest = abs(diffLoc.z);
			offset = vec3(0, 0, glm::sign(diffLoc.z));
		}

		newBlockSlot = blockLoc + offset;
		faceCenter = blockLoc + offset / 2.0f;

		if (lmbPressed)
		{
			m_landscapePtr->CreateBlock(newBlockSlot.x, newBlockSlot.y, newBlockSlot.z, 1 + rand() % 5);
		}
	}
}

//Draw the player object.
void Player::Draw()
{
	//No need to draw anything at the moment.
	if (intersection)
	{
		Shader::GetCurrentProgramShader()->SetInt("UseTexture", 0);

		Mesh* cube1 = ResourceManager::Instance().FindMesh("SelectCube");
		Mesh* cube2 = ResourceManager::Instance().FindMesh("FaceCube");

		Shader::GetCurrentProgramShader()->SetVec3("inPos", newBlockSlot);

		glEnable(GL_BLEND);
		glDepthMask(GL_FALSE);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		cube1->Draw();

		Shader::GetCurrentProgramShader()->SetVec3("inPos", faceCenter);

		cube2->Draw();

		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}
}

vec3 Player::GetVelocity()
{
	return m_velocity;
}

bool Player::GetOnGround()
{
	return m_onGround;
}
