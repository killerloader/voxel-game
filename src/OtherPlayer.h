#pragma once
#include "WorldData.h"
#include "Mesh.h"
#include "BoxCollider.h"

class OtherPlayer : public WorldData
{
public:
	OtherPlayer();
	~OtherPlayer();

	void Update(float dt);
	void Draw();

	void SetVelocity(vec3 velocity);

private:
	vec3 m_velocity;
	Mesh* m_playerMesh;
};