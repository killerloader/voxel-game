#include "BaseMap.h"

BaseMap::BaseMap()
{
	m_useDeferred = true;
	m_usePostProcessing = true;
}

BaseMap::~BaseMap()
{
}

void BaseMap::Draw() {}
void BaseMap::DrawUI() {}

/*
	Checks whether a map has deferrred rendering enabled.
	Deferred rendering can only be used if the bound shader does not change. Also the vertex information needs to be what the shader expects.
	Code placed in DrawGPass() will be used by deferred rendering.
*/
bool BaseMap::UsesDeferred()
{
	return m_useDeferred;
}

/*
	Checks whether a map has post processing enabled.
	Any map can use it as long as the map doesn't bind a different framebuffer.
	Code must be placed in the standard Draw() method.
	Post processing can be altered in the DrawPost() method.
*/
bool BaseMap::UsesPostProcessing()
{
	return m_usePostProcessing;
}
