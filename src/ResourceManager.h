#pragma once
#include <vector>
#include <string>

class Mesh;
class Shader;
class Texture;

//Resource manager, for handling and destroying objects.
class ResourceManager
{
public:
	static ResourceManager& Instance();			//Single access
	static void DestroyInstance();	//Destroy singleton.

	//Meshes
	bool AddMesh(Mesh* mesh, std::string name);	//Add a mesh.
	bool AddMesh(const std::vector<Mesh*> mesh, std::string name);	//Add an array of meshes.
	std::vector<Mesh*> FindMeshes(std::string name);	//Find an array of meshes.
	Mesh* FindMesh(std::string name);	//Find a single mesh.

	bool AddTexture(Texture* texture, std::string name);	//Add a texture.
	Texture* FindTexture(std::string name);					//Find a texture.

	bool AddShader(Shader* shader, std::string name);		//Add a shader.
	Shader* FindShader(std::string name);					//Find a shader.

private:
	ResourceManager();	//Private constructor, so cannot be created outside of singleton.
	~ResourceManager();	//Private deconstructor, so cannot be destroyed outside of singleton.
	ResourceManager(const ResourceManager& other) = delete;				//Deleted copy constructor.
	ResourceManager& operator=(const ResourceManager& other) = delete;	//Deleted copy assignment operator.

	static ResourceManager* m_instance;	//Internal singleton instance pointer.

	//Templated struct ofr different items.
	template<typename dataType>
	struct ItemGroup { dataType m_data; std::string m_name; };

	//Arrays of items grouped with their names.
	std::vector<ItemGroup<std::vector<Mesh*>>> m_meshes;	//meshes.
	std::vector<ItemGroup<Texture*>> m_textures;		//textures.
	std::vector<ItemGroup<Shader*>> m_shaders;		//Shaders.
};