#include "MinecraftMap.h"
#include "Application.h"
#include "Shader.h"
#include "Mesh.h"
#include "PlayerCamera.h"
#include "Landscape.h"
#include "LandscapeChunk.h"
#include "Player.h"
#include "Client.h"
#include "CollisionRay.h"
#include "BoxCollider.h"
#include <iostream>

MinecraftMap::MinecraftMap()
{
	//Create landscape.
	m_landscape = new Landscape();
	m_landscape->SetCentralPoint(0,0);

	//create and setup camera,
	m_camera = new PlayerCamera();
	m_camera->SetPerspective(glm::pi<float>() * 0.25f, 16 / 9.f, 0.2f, 3000.f);
	m_camera->GetTransform().SetPosition(vec3(8, 250, 8));
	m_camera->GetTransform().LookDirection(glm::normalize(-vec3(16, 250, 16)), vec3(0, 1, 0));

	//Create player.
	m_player = new Player(*m_camera);

	m_player->SetLandscapeInstance(m_landscape);

	m_camera->SetTarget(&m_player->GetTransform());

	//Find the shader used.
	m_shader = ResourceManager::Instance().FindShader("McShader");
	m_shaderNormal = ResourceManager::Instance().FindShader("McShaderNormal");

	m_useDeferred = false;
	m_usePostProcessing = false;

	m_client = new Client();
	m_client->Connect();
}

//Deconstructor. Delete poitners.
MinecraftMap::~MinecraftMap()
{
	delete m_player;
	delete m_landscape;
	delete m_camera;
	delete m_client;
}

//Update method. update player and map/
void MinecraftMap::Update(float dt)
{
	glfwSetInputMode(Application::Instance().GetWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	//Player update first, so camera has correct position.
	m_player->Update(dt);

	if (Application::Instance().IsWindowFocused())
		m_camera->UpdateMousePos(Application::Instance().GetMousePosition());

		m_camera->Update(Application::Instance().GetWindow(), dt);

		vec2 centerPos = Application::Instance().GetFramebufferSize() / 2.0f;

		glfwSetCursorPos(Application::Instance().GetWindow(), centerPos.x, centerPos.y);

	if (Application::Instance().IsWindowFocused())
		m_camera->UpdateMousePos(centerPos);

	//Return to menu.
	if (glfwGetKey(Application::Instance().GetWindow(), GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(Application::Instance().GetWindow(), 1);
		
		return;
	}

	m_client->Update(dt);
	m_client->SendPlayerData(m_player);
}

//Draw UI.
void MinecraftMap::DrawUI()
{
	ImVec2 size(200, 300);
	ImGui::SetNextWindowPos(ImVec2(32, 32));
	ImGui::SetNextWindowSize(size);
	ImGui::SetNextWindowSizeConstraints(size, size);

	//Being ImGUI stuff.
	ImGui::Begin("Debug");

	ImGui::TextWrapped("Controls: \n\
- Mouse to look around. \n\
- Left Mouse Button to place a block. \n\
- Right Mouse Button to destroy a block. \n\
- X to place a block below the player. \n\
- Z to destroy many blocks around the player. \n\
- W,A,D,S to move.\n\
- Space to jump. \n\
- Escape to return to the main menu.");

	ImGui::End();

	ImGui::Render();
}

//Draw the minecaft map. Branches off into landscape and player draw methods.
void MinecraftMap::Draw()
{
	//Clear the sky.
	//glClearColor(0.7f, 0.8f, 1.0f, 1);

	glClearColor(0.7f, 0.7f, 1.0f, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_shader->Bind();

	m_shader->SetInt("UseTexture", 0);

	m_shader->SetMat4("projectionViewWorldMatrix", m_camera->WorldProjectionView());
	vec4 light = (m_player->GetTransform().GetWorldTransform())[3];
	m_shader->SetVec4("LightPos", light);

	m_shader->SetFloat("LinearGI", LINEARGI);
	//m_shader->SetFloat("ExponentialGI", EXPONENTIALGI);

	m_landscape->SetCentralPoint(m_player->GetTransform().GetPosition().x / 16.0f, m_player->GetTransform().GetPosition().z / 16.0f);
	
	m_landscape->Draw(m_camera);

	m_shaderNormal->Bind();

	m_shaderNormal->SetMat4("projectionViewWorldMatrix", m_camera->WorldProjectionView());
	m_shaderNormal->SetVec4("LightPos", light);

	m_player->Draw();

	m_client->Draw();
}