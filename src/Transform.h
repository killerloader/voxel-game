#pragma once
#include "GL/gl3w.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 
#include <glm/ext.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;
using glm::quat;

/*
	Transform class.
	Holds information about rotation, scale and translation.
	Recompiles these components into a world transform when it is needed.
	Compiles the components in an order so that 
*/
class Transform
{
public:
	Transform();	//Constructor
	~Transform();	//Destructor

	void Translate(vec3& translation);			//Method to translate transform.
	void SetPosition(vec3& position);			//Method to set the position of the transform.
	vec3& GetPosition();

	void LookDirection(vec3 direction, vec3 up);	//Looks in a direction using a normalized vector. Set up for use with cameras.
	void Rotate(vec3& axis, float rotation);	//Method to rotate transform around a specified axis.
	quat& GetRotation();
	
	void SetScale(vec3& scale);					//Method to set the scale of the transform directly.
	void Scale(vec3& scale);					//Method to scale transform.
	vec3 GetScale();

	vec3 Forward();	//Gets the transform's forward vector.
	vec3 Right();	//Gets the transform's right vector.
	vec3 Up();		//Gets the transform's up vector.

	const mat4& GetWorldTransform();	//Returns the world transform matrix.

	bool IsDirty();			//Returns if the transform needs to be updated.
	void MakeDirty();		//Forces the transform to be dirty, useful if changing transforms through references to internal transforms.
	void ForceUpdate();		//Forces the transform to update and be no longer dirty.

	static vec3 LocalToGlobal(vec3 localVector, Transform& transform);	//Converts a local vector to a global vector.
	static vec3 GlobalToLocal(vec3 globalVector, Transform& transform);	//Converts a global vector to a local vector.
private:
	quat m_rotation;			//Rotation of transform as quaternion.
	mat4 m_translation;			//Translation of transform.
	mat4 m_scale;				//Scale of transform.
	mat4 m_worldTransform;		//World transform, the compilation of the three components above.
	bool m_dirty;				//If the world transform needs to be recompiled.
	bool m_updated;				//Similar to dirty, but used from the outside.
};