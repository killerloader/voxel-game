#pragma once
#include <glm/glm.hpp> 
#include <glm/ext.hpp>
#include <glm/gtc/noise.hpp>
#include <mutex>
#include <list>

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

class LandscapeChunk;
struct LandscapeBlock;
class Shader;
class BoxCollider;
class CollisionRay;
class Texture;

//View distance/ amount of chunks to create and render.
#define RENDER_DIST 32
#define CHUNKS (1 + RENDER_DIST  * 2)

class PlayerCamera;

//Landscape class. For handling many chunks.
class Landscape
{
public:
	Landscape();	//Constrcutor.
	~Landscape();	//Destructor.

	void SetupBlocks();	//Setup templates for block types.

	bool CheckBlock(int x, int y, int z);	//Check if block exists in global space.
	unsigned char GetBlock(int x, int y, int z);	//Get a block ID in global space.
	void DestroyBlock(int x, int y, int z);	//Destroy a block in global space.
	void CreateBlock(int x, int y, int z, unsigned char blockType);	//Create a block in global space.
	LandscapeChunk* GlobalSpaceToLocalChunk(int x, int z);
	LandscapeChunk* GlobalChunkToLocalChunk(int x, int z);

	LandscapeChunk* GetChunk(int X, int Y);

	LandscapeBlock& GetBlockType(int ID);	//Get information on a block type. (template)

	std::mutex& GetChunkMutex();

	//if force is true, the program will block with a mutex until the central point is set.
	//Otherwise it will wait until no threads are trying to access the chunks.
	void SetCentralPoint(int sX, int sY, bool force = false);

	void Draw(PlayerCamera* camera);	//Draw

	bool CheckCollision(BoxCollider& collider, vec3* overlap = nullptr);	//Check for collision in global space.
	bool CheckCollision(CollisionRay& collider, float length = 10.0f, vec3* intersection = nullptr, vec3* blockLocation = nullptr);	//Check for raycast collision in global space.
	bool CheckCollisionOffset(BoxCollider& collider, vec3 offset, vec3* overlap = nullptr);	//Check for a collision given an offset.

private:
	LandscapeChunk*** m_chunks;	//2D array of chunk pointers.
	Texture* m_landTextureAtlas;	//Texture atlas for MC textures.
	LandscapeBlock* m_blockTypes;	//Array of block types.

	std::mutex m_chunkMutex;

	std::list<LandscapeChunk*> m_trashChunks;

	int m_cCenterX, m_cCenterY;
};