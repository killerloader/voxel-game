#include "CollisionRay.h"
#include "BoxCollider.h"
#include <iostream>

CollisionRay::CollisionRay(vec3 pos, vec3 dir)
{
	m_position = pos;
	m_direction = dir;
}

CollisionRay::~CollisionRay()
{
}

bool CollisionRay::CheckAgainst(const BoxCollider & boxCollider, float length, vec3* intersection)
{
	float tmin = 0;
	float tmax = length;

	//Loop through all axis.
	for (int i = 0; i < 3; i++)
	{
		float invD = 1.0f / m_direction[i];
		float t0 = (boxCollider.GetBounds().min[i] - m_position[i]) * invD;
		float t1 = (boxCollider.GetBounds().max[i] - m_position[i]) * invD;
		if (invD < 0.0f)
		{//Swap values
			float tmp = t0;
			t0 = t1;
			t1 = tmp;
		}

		tmin = t0 > tmin ? t0 : tmin;
		tmax = t1 < tmax ? t1 : tmax;

		//Rules out collision.
		if (tmax <= tmin)
			return false;

		//Continue to next axis test.
	}

	if (intersection != nullptr)
		*intersection = m_position + tmin * m_direction;

	return true;
}

vec3 CollisionRay::GetPosition()
{
	return m_position;
}

vec3 CollisionRay::GetDirection()
{
	return m_direction;
}
