#pragma once
#include "GL/gl3w.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 
#include <glm/ext.hpp>
#include <glm/gtc/noise.hpp>
#include <vector>

#include "ResourceManager.h"

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

//The type of mesh. Whether it uses an index buffer or not.
enum EMeshType
{
	EMeshType_VBO,
	EMeshType_IBO
};

//A single vertex
struct Vertex
{
	vec4 position;
	vec4 normal;
	vec4 tangent;
	vec4 color;
	vec2 uv;
};

//Mesh class, holds information about a single mesh.
//Contains many static factory methods for creating meshes.
class Mesh
{ 
public:
	//Doesn't handle any shaders.
	void Draw();

	//Generate a generic cube.
	static Mesh* GenerateCube(std::string meshName, float length = 1.0f, float width = 1.0f, float height = 1.0f, vec2 repeat = vec2(1,1), vec4 colour = vec4(1));

	//Generate a flat plane (horizontal)
	static Mesh* GeneratePlane(std::string meshName, float width = 1.0f, float height = 1.0f, vec2 repeat = vec2(1), vec4 colour = vec4(1));

	//Generate a fullscreen quad.
	static Mesh* GenerateFullscreenQuad(std::string meshName, int screenWidth, int screenHeight);

	//Calculate a tangent given three vertices.
	static vec3 CalculateTangent(Vertex& v1, Vertex& v2, Vertex& v3);

	//Generate and load a model from a file.
	static std::vector<Mesh*> GenerateModel(std::string meshName, std::string fileName);

	//Generate and load two models from two files, and create a morphing animation model.
	static std::vector<Mesh*> GenerateMorphingAnimationModel(std::string meshName, std::string fileName, std::string fileName2);

private:
	unsigned int m_VBO;			//Main vertex buffer object.
	unsigned int m_IBO;			//Main Index buffer object (if used)
	unsigned int m_VAO;			//Main Vertex Array Object.
	unsigned int m_indexCount;	//Index count (when using IBO)
	unsigned int m_vertexCount;	//Vertex count (when not using IBO)
	bool m_morphing;			//If this mesh is a morphing mesh (used for mophing animations)
	unsigned int m_VBO2;		//Secondary vertex buffer object (used for morphing meshes as second model)
	EMeshType m_type;			//Type of mesh. (whether it uses an IBO or a VBO)

	//Private constructor.
	Mesh();

	//Delete copy methods, so mesh cannot be copied.
	Mesh(const Mesh& other) = delete;
	Mesh& operator=(const Mesh& other) = delete;
	~Mesh();	//private Deconstructor, so only resource manager can destroy.

	//Allow resource manager to delete this mesh.
	friend ResourceManager;
};