#include "Texture.h"
#include "ResourceManager.h"
#include "other/STB_Image.h"
#include <vector>

//Constructor. set default.
Texture::Texture()
{
	m_destroyResource = true;
}

//Destructor. Destroys the texture.
Texture::~Texture()
{
	if (m_destroyResource)
		glDeleteTextures(1, &m_textureID);
}

//Get the internal texture ID.
int Texture::GetTextureID()
{
	return m_textureID;
}

//Loads a texture from a file, stores it in the resource manager given a name, and returns a point to it.
Texture* Texture::LoadTexture(std::string textureName, std::string fName, int TextureType, int FilterType)
{
	unsigned char* data = nullptr;

	int imageWidth = 0, imageHeight = 0, imageFormat = 0;
	data = stbi_load(fName.c_str(), &imageWidth, &imageHeight, &imageFormat, STBI_default);

	//Data failed to load
	if (data == nullptr)
		return nullptr;

	unsigned int tex;

	//Generate the texture.
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexImage2D(GL_TEXTURE_2D, 0, TextureType, imageWidth, imageHeight, 0, TextureType, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(data);

	Texture* newTex = new Texture();

	newTex->m_textureID = tex;

	//Couldn't add texture.
	if (!ResourceManager::Instance().AddTexture(newTex, textureName))
	{
		delete newTex;
		return nullptr;
	}

	return newTex;
}

//Manually import a texture ID (Possibly from a framebuffer)
//Becuase the texture is owned by the framebuffer and not the texture class, it works differently.
//You must call DestroyTemporary() to destroy the texture class, and the texture will not be destroyed unless its owner destroys it.
Texture* Texture::ImportTextureID(unsigned int TextureID)
{
	Texture* newTexture = new Texture();
	newTexture->m_textureID = TextureID;
	newTexture->m_destroyResource = false;
	return newTexture;
}

//Destroy a temporary texture. (Wont delete the actual texture, but only the class)
void Texture::DestroyTemporary()
{
	if (!m_destroyResource)
		delete this;
}
