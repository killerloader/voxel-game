#pragma once
#include "WorldData.h"
#include "Mesh.h"
#include "BoxCollider.h"

/*
	This controls how many times the distance to push out gets halved when the player becomes stuck in an object.
	This is only used in specific situations where a player falls through an object and needs to be forced out.
	The method uses a binary approach and halves the distance to push each time.

	The higher the number is, the more natural it will look like when being pushed out of an object. The number is also exponential, so the number doesn't have to be large for it to be perfect.
	The number is directly proportional to how many collision checks occur, so its efficiency is o(n)
*/
#define PUSHOUT_ITERATIONS 5

class Shader;
class BaseCamera;
class Landscape;

/*
	Class to hold minecraft player.
	Inherits from worlddata to use a transform.
*/
class Player : public WorldData
{
public:
	Player(BaseCamera& camera);	//Constructor.
	~Player();	//Destructor.

	void SetLandscapeInstance(Landscape* lndscpPtr);	//Set the internal landscape ptr for collision purposes.

	void Update(float dt);	//Update. Controls player movement and collision/ interaction.
	void UpdateBlockPlacement();	//Calculate which block the player is looking at.
	void Draw();	//Empty draw method. In case the player should draw weapons in the future.

	vec3 GetVelocity();
	bool GetOnGround();
private:
	BoxCollider m_boxCollider;	//Player's collider, used to collide with world.
	BaseCamera& m_camera;		//Reference to camera used in minecraft world. Use to move player along camera's axis.
	bool m_onGround;			//If the player is on the ground.
	vec3 m_velocity;			//The stored velocity of the player.
	Landscape* m_landscapePtr;	//Pointer to landscape object.

};