#pragma once
#include <glm/glm.hpp> 
#include <glm/ext.hpp>
#include <glm/gtc/noise.hpp>
#include "WorldData.h"

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

//Simple bounds structure.
struct Bounds
{
	vec3 min;
	vec3 max;
};

//Box collider class for basic AABB collision.
class BoxCollider : public WorldData
{
public:
	BoxCollider(vec3 size);	//Constructor to create collider.
	~BoxCollider();	//Destructor.

	void SetTransformation(const mat4& matrix);		//Transform the bounds based on a matrix. (resets transform)
	void SetTranslation(vec3& translation);			//Transform the bounds based on a vector of translation. (resets transform)
	void Translate(vec3& translate);				//Translate the transformed bounds.

	bool CheckAgainst(BoxCollider& other, vec3* overlap = nullptr);	//Check collision against another collider, also gives minimum translation vector.

	Bounds GetBounds() const;	//Get the transformed bounds.

private:
	Bounds m_bounds;				//Original bounds.
	Bounds m_transformedBounds;		//Bounds after transformation.
};