#include "OtherPlayer.h"
#include "ResourceManager.h"
#include "Shader.h"

OtherPlayer::OtherPlayer()
{
	m_playerMesh = ResourceManager::Instance().FindMesh("PlayerCube");
}

OtherPlayer::~OtherPlayer()
{
}

void OtherPlayer::Update(float dt)
{
}

void OtherPlayer::Draw()
{
	Shader::GetCurrentProgramShader()->SetVec3("inPos", m_transform.GetPosition() + vec3(0,0.25f,0));
	Shader::GetCurrentProgramShader()->SetInt("UseTexture", 0);
	m_playerMesh->Draw();
}

void OtherPlayer::SetVelocity(vec3 velocity)
{
	m_velocity = velocity;
}
