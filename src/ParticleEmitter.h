#pragma once

#include "GL/gl3w.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 
#include <glm/ext.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

//A single particle struct.
struct Particle 
{ 
	vec3 position; 
	vec3 velocity; 
	vec4 colour; 
	float size; 
	float lifetime; 
	float lifespan; 
}; 

//Particle vertex. The information for a single vertex.
struct ParticleVertex 
{ 
	vec4 position; 
	vec4 colour; 
};

class BaseCamera;

/*
	Particle emitter class used for creating particle effets.
*/
class ParticleEmitter
{
public:
	ParticleEmitter();	//Constructor.
	virtual ~ParticleEmitter();	//Destructor.

	//Initializes the particle system.
	void Initalise(unsigned int maxParticles, unsigned int emitRate, float lifetimeMin, float lifetimeMax, float velocityMin, float velocityMax, float startSize, float endSize, const vec4& startColour, const vec4& endColour);
	void Emit();	//Emit a particle if possible.
	void Update(float dt, BaseCamera* camera);	//Update the particles.
	void SetPosition(vec3 position);
	void Draw();	//Draw the particles.
private:
	Particle* m_particles;			//Pointer for array of particles.
	ParticleVertex* m_vertexData;	//Pointer for array of particle vertices.

	unsigned int m_firstDead;			//Identifier to remember the ID of the first dead particle.
	unsigned int m_maxParticles;		//Maximum amount of allowed particles. (for pooling)
	unsigned int m_vao, m_vbo, m_ibo;	//VBO/ VAO/ IBO data for particles.

	vec3 m_position;		//Position of emitter.
	float m_emitTimer; 		//Timer for emitter, to control when particles spawn.
	float m_emitRate; 		//How many particles get emitter per second.
	float m_lifespanMin; 	//Minimum particle lifespan.
	float m_lifespanMax; 	//Maximum particle lifespan.
	float m_velocityMin; 	//Minimum particle velocity.
	float m_velocityMax; 	//Maximum particle velocity.
	float m_startSize; 		//Start size for particle.
	float m_endSize; 		//End size for particle.
	vec4 m_startColour; 	//Start colour for particle.
	vec4 m_endColour;		//End colour for particle.
};