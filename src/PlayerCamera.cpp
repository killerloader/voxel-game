#include "PlayerCamera.h"
#include "Transform.h"

//Constructor. Sets default values.
PlayerCamera::PlayerCamera()
{
	m_playerTransform = nullptr;
	m_storedMove = vec2(0);
}

//Destructor.
PlayerCamera::~PlayerCamera()
{
}

//Update method.
//Handles camera movement and rotation based on mouse movement.
void PlayerCamera::Update(GLFWwindow* window, float dt)
{
	float mouseRotMult = 0.001f;
	float rotSpd = 0.03f;

	vec2 changePos = m_oldMousePos - m_mousePos;

	//Reset rotation so we can redo it (easier to limit maximum rotations)
	m_transform.GetRotation() = quat();
	//Because we are setting the rotation directly like this, we need to force an update on the internal world transform in the transform.
	m_transform.ForceUpdate();

	//Rotate camera around global y axis.
	if (changePos.x != 0)
		m_storedMove.x += changePos.x * dt * 0.2f;

	//Rotate camera around local x axis.
	if (changePos.y != 0)
		m_storedMove.y += changePos.y * dt * 0.2f;

	m_storedMove.y = glm::clamp(m_storedMove.y, -glm::pi<float>() / 2.0f * 0.98f, glm::pi<float>() / 2.0f * 0.98f);

	m_actualMove += (m_storedMove - m_actualMove) * 0.3f;

	m_transform.Rotate(Transform::LocalToGlobal(vec3(0, 1, 0), m_transform), m_actualMove.x);

	m_transform.Rotate(vec3(1, 0, 0), m_actualMove.y);

	//Set position relative to player to make camera look at player.
	m_transform.SetPosition(vec3(m_playerTransform->GetWorldTransform()[3]) + vec3(0, 0.65f, 0));
}

//Gives the player camera access to mouse position. This can be called twice to make the camera think the mouse didn't move while changing its position.
void PlayerCamera::UpdateMousePos(vec2 pos)
{
	if (!firstMouseEvent)
	{
		firstMouseEvent = true;
		m_mousePos = pos;
	}

	m_oldMousePos = m_mousePos;
	m_mousePos = pos;
}

//Set a target transform for the camera. The camera will follow it.
void PlayerCamera::SetTarget(Transform * transform)
{
	m_playerTransform = transform;
}
