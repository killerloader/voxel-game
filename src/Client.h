#pragma once

#include <iostream> 
#include <string> 
#include <string> 
#include <RakPeerInterface.h> 
#include <MessageIdentifiers.h> 
#include <BitStream.h>

#define PLAYERCOUNT 32

class OtherPlayer;
class Player;

class Client
{
public:
	Client();
	~Client();

	void Connect();
	void Update(float dt);
	void Draw();
	bool IsConnected();
	void SendPlayerData(Player* plr);

	OtherPlayer** GetOtherPlayers();
private:
	void HandleNetworkMessages();

	RakNet::RakPeerInterface* m_peerInterface;
	const char* IP = "127.0.0.1";
	const unsigned short PORT = 5456;
	unsigned char m_ID;
	RakNet::SystemAddress m_serverAddr;
	bool m_connected;
	bool m_gotID;

	OtherPlayer* m_otherPlayers[PLAYERCOUNT];
};