#include "ParticleEmitter.h"
#include "BaseCamera.h"

//Particle emitter constructor.
//Setup default variables.
ParticleEmitter::ParticleEmitter()
{
	//Setup default variables.
	m_particles = nullptr;
	m_firstDead = 0;
	m_maxParticles = 0;
	m_position = vec3(0);
	m_vao = 0;
	m_vbo = 0;
	m_ibo = 0;
	m_vertexData = nullptr;
}

//Destructor. Destroys buffers and arrays.
ParticleEmitter::~ParticleEmitter()
{
	delete[] m_particles;
	delete[] m_vertexData;
	glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_ibo);
}

//Initializes the particle emitter so it can emit the specified particles using many parameters.
void ParticleEmitter::Initalise(unsigned int maxParticles, unsigned int emitRate, float lifetimeMin, float lifetimeMax, float velocityMin, float velocityMax, float startSize, float endSize, const vec4 & startColour, const vec4 & endColour)
{
	// set up emit timers 
	m_emitTimer = 0; 
	m_emitRate = 1.0f / emitRate; 
	
	// store all variables passed in 
	m_startColour = startColour; 
	m_endColour = endColour; 
	m_startSize = startSize; 
	m_endSize = endSize; 
	m_velocityMin = velocityMin; 
	m_velocityMax = velocityMax; 
	m_lifespanMin = lifetimeMin; 
	m_lifespanMax = lifetimeMax;
	m_maxParticles = maxParticles; 
	
	// create particle array 
	m_particles = new Particle[m_maxParticles]; 
	m_firstDead = 0; 
	
	// create the array of vertices for the particles 
	// 4 vertices per particle for a quad. 
	// will be filled during update 
	m_vertexData = new ParticleVertex[m_maxParticles * 4];

	// create the index buffeer data for the particles
	// 6 indices per quad of 2 triangles
	// fill it now as it never changes 
	unsigned int* indexData = new unsigned int[m_maxParticles * 6];
	for (unsigned int i = 0; i < m_maxParticles; ++i) 
	{ 
		indexData[i * 6 + 0] = i * 4 + 0;
		indexData[i * 6 + 1] = i * 4 + 1;
		indexData[i * 6 + 2] = i * 4 + 2;
		indexData[i * 6 + 3] = i * 4 + 0;
		indexData[i * 6 + 4] = i * 4 + 2;
		indexData[i * 6 + 5] = i * 4 + 3; 
	} 
	// create opengl buffers 
	glGenVertexArrays(1, &m_vao); 
	glBindVertexArray(m_vao); 
	glGenBuffers(1, &m_vbo); 
	glGenBuffers(1, &m_ibo); 
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo); 

	//use GL_DYNAMIC_DRAW, this is just a hint for the drivers, it may not actually change anything, but it can allow for optimizations.
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * 4 * sizeof(ParticleVertex), nullptr, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo); 
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_maxParticles * 6 * sizeof(unsigned int), indexData, GL_STATIC_DRAW); 

	// position 
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), 0);
	glEnableVertexAttribArray(0);
	
	// colour 
	glEnableVertexAttribArray(1); 
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), ((char*)0) + 16); 
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0); 
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 

	delete[] indexData;
}

//Attemtps to emit a single particle if there is room.
void ParticleEmitter::Emit()
{
	// only emit if there is a dead particle to use 
	if (m_firstDead >= m_maxParticles) 
		return; 
	
	// resurrect the first dead particle 
	Particle& particle = m_particles[m_firstDead++]; 

	// assign its starting position 
	particle.position = m_position; 

	// randomise its lifespan
	particle.lifetime = 0;
	particle.lifespan = (rand() / (float)RAND_MAX) * (m_lifespanMax - m_lifespanMin) + m_lifespanMin; 
	
	// set starting size and colour 
	particle.colour = m_startColour;
	particle.size = m_startSize; 
	
	// randomise velocity direction and strength 
	float velocity = (rand() / (float)RAND_MAX) * (m_velocityMax - m_velocityMin) + m_velocityMin; 
	particle.velocity.x = (rand() / (float)RAND_MAX) * 2 - 1; 
	particle.velocity.y = (rand() / (float)RAND_MAX) * 2 - 1; 
	particle.velocity.z = (rand() / (float)RAND_MAX) * 2 - 1; 
	particle.velocity = glm::normalize(particle.velocity) * velocity;
}

//Updates the particles, and emits them if needed.
//Updates only the alive particles and ignores dead ones.
//Recreates buffers each frame. (reinputs vertex data)
void ParticleEmitter::Update(float dt, BaseCamera * camera)
{
	// spawn particles 
	m_emitTimer += dt; 
	
	while (m_emitTimer > m_emitRate) 
	{ 
		Emit(); 
		m_emitTimer -= m_emitRate; 
	} 
	unsigned int quad = 0; 
	
	// update particles and turn live particles into billboard quads 
	for (unsigned int i = 0; i < m_firstDead; i++) 
	{ 
		Particle* particle = &m_particles[i]; 
		particle->lifetime += dt; 
		if (particle->lifetime >= particle->lifespan) 
		{ 
			// swap last alive with this one 
			*particle = m_particles[m_firstDead - 1]; 
			m_firstDead--; 
		} 
		else 
		{ 
			// move particle 
			particle->position += particle->velocity * dt; 

			// size particle 
			particle->size = glm::mix(m_startSize, m_endSize, particle->lifetime / particle->lifespan); 
			
			// colour particle 
			particle->colour = glm::mix(m_startColour, m_endColour, particle->lifetime / particle->lifespan);

			// make a quad the correct size and colour 
			float halfSize = particle->size * 0.5f; 
			m_vertexData[quad * 4 + 0].position = vec4(halfSize, halfSize, 0, 1); 
			m_vertexData[quad * 4 + 0].colour = particle->colour; 
			m_vertexData[quad * 4 + 1].position = vec4(-halfSize, halfSize, 0, 1); 
			m_vertexData[quad * 4 + 1].colour = particle->colour; 
			m_vertexData[quad * 4 + 2].position = vec4(-halfSize, -halfSize, 0, 1); 
			m_vertexData[quad * 4 + 2].colour = particle->colour; 
			m_vertexData[quad * 4 + 3].position = vec4(halfSize, -halfSize, 0, 1); 
			m_vertexData[quad * 4 + 3].colour = particle->colour; 
			// create billboard transform 
			vec3 cameraTrans = camera->GetTransform().GetPosition();

			//Camera position - particle position (direction to particle)
			vec3 zAxis = glm::normalize(cameraTrans - particle->position);

			//Cross between camera up vector and direction to particle to get the right vector.
			//Not the same as camera right vector!
			vec3 xAxis = glm::cross(camera->GetTransform().Right(), zAxis);

			//Cross between right vector and forward vector to get up vector.
			//Not the same as camera up vector!
			vec3 yAxis = glm::cross(zAxis, xAxis); 
			glm::mat4 billboard(vec4(xAxis, 0), vec4(yAxis, 0), vec4(zAxis, 0), vec4(0,0,0,1)); 
			m_vertexData[quad * 4 + 0].position = billboard * m_vertexData[quad * 4 + 0].position + vec4(particle->position,0); 
			m_vertexData[quad * 4 + 1].position = billboard * m_vertexData[quad * 4 + 1].position + vec4(particle->position,0); 
			m_vertexData[quad * 4 + 2].position = billboard * m_vertexData[quad * 4 + 2].position + vec4(particle->position,0); 
			m_vertexData[quad * 4 + 3].position = billboard * m_vertexData[quad * 4 + 3].position + vec4(particle->position,0); 
			++quad; 
		} 
	}
}

void ParticleEmitter::SetPosition(vec3 position)
{
	m_position = position;
}

//Draw the particle system.
void ParticleEmitter::Draw()
{
	// sync the particle vertex buffer 
	// based on how many alive particles there are 
	// May be better to do this in update, and make it so that it only updates a single vertex.
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo); 
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_firstDead * 4 * sizeof(ParticleVertex), m_vertexData); 
	
	// draw particles 
	glBindVertexArray(m_vao); 
	glDrawElements(GL_TRIANGLES, m_firstDead * 6,GL_UNSIGNED_INT, 0);
}
