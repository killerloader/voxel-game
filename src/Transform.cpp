#include "Transform.h"

//Constructor. Set default values.
Transform::Transform()
{
	m_dirty = false;
}

Transform::~Transform()
{

}

//Translate the transform.
void Transform::Translate(vec3& translation)
{
	//Translate and set dirty.
	m_translation = glm::translate(m_translation, translation);
	m_dirty = true;
}

//Scale the transform.
void Transform::Scale(vec3& scale)
{
	//Scale and set dirty.
	m_scale = glm::scale(scale);
	m_dirty = true;
}

//Look direction, this is similar to glm::lookAt, but works in world transform.
//If using world transform, this will actually make an object look in the opposite direction. (As it is optimized for cameras)
//This can be fixed by inverting the direction.
void Transform::LookDirection(vec3 direction, vec3 up)
{
	//Convert lookAt into a world transform. Also set dirty.
	m_rotation = glm::conjugate(glm::toQuat(glm::lookAt(vec3(0), direction, up)));
	m_dirty = true;
}

//Rotate the transform around an axis.
void Transform::Rotate(vec3& axis, float rotation)
{
	//Rotate and set dirty.
	m_rotation = glm::rotate(m_rotation, rotation, axis);
	m_dirty = true;
}

//Set the position of the transform.
void Transform::SetPosition(vec3& position)
{
	//Set position and set dirty.
	m_translation = glm::translate(position);
	m_dirty = true;
}

//Set the scale of the transform.
void Transform::SetScale(vec3& scale)
{
	//set scale and set dirty.
	m_scale = glm::scale(m_scale, scale);
	m_dirty = true;
}

//Get the world transform of this matrix.
//If the transform is dirty, this will create a new matrix.
const mat4 & Transform::GetWorldTransform()
{
	//Check if matrix needs update, if it does, force an update.
	if (m_dirty)
		ForceUpdate();

	//return world transform matrix.
	return m_worldTransform;
}

//Get the position of the matrix.
//This returns a vector 3 reference of the position so it is possible to directly edit it.
//This is due to the translation components being next to each other, and therefore possible to map into the memory space of a vector 3.
vec3 & Transform::GetPosition()
{
	//Get a reference to the translation component inside the translation matrix.
	return *(vec3*)(&(m_translation[3]));
}

//Return the scale of the transform.
vec3 Transform::GetScale()
{
	//Scale along diagonal, so impossible to get a reference to these values.
	return vec3(m_scale[0][0], m_scale[1][1], m_scale[2][2]);
}

//Return forward vector of transform.
vec3 Transform::Forward()
{
	//Check if need update before getting data.
	if (m_dirty)
		ForceUpdate();

	//Get world transform so that it is up to date (no longer dirty)
	return vec3(GetWorldTransform()[2]);
}

//Return right vector of transform.
vec3 Transform::Right()
{
	//Check if need update before getting data.
	if (m_dirty)
		ForceUpdate();

	//Get world transform so that it is up to date (no longer dirty)
	return vec3(GetWorldTransform()[0]);
}

//Return up vector of transform.
vec3 Transform::Up()
{
	//Check if need update before getting data.
	if (m_dirty)
		ForceUpdate();

	//Get world transform so that it is up to date (no longer dirty)
	return vec3(GetWorldTransform()[1]);
}

//Check if the transform is dirty or not.
bool Transform::IsDirty()
{
	return m_dirty;
}

//Force the transform to be dirty.
//This will make the world transform force an update when it is required next.
//This can be used if directly editing values. (Using getRotation or getPosition and editing them)
void Transform::MakeDirty()
{
	m_dirty = true;
}

//Force an update.
//This will update the world transform even if it is not dirty.
//Can be used for similar reasons to MakeDirty().
void Transform::ForceUpdate()
{
	m_dirty = false;

	//Recalculate transform.
	//Use TSR, so that rotation and scale is local instead of global.
	m_worldTransform = m_translation * m_scale * glm::toMat4(m_rotation);
}

//Convert a local directional vector into a global directional vector.
vec3 Transform::LocalToGlobal(vec3 localVector, Transform& transform)
{
	return vec3(vec4(localVector, 0) * transform.GetWorldTransform());
}

//Convert a global directional vector into a local directional vector.
vec3 Transform::GlobalToLocal(vec3 globalVector, Transform & transform)
{
	return vec3(vec4(globalVector, 0) * glm::inverse(transform.GetWorldTransform()));
}

//Get a reference to the quaternion rotation of this transform.
//This can be edited directly.
//Although if it is edited, the transform will not know to make itself dirty, so it is suggested to use either MakeDirty() or ForceUpdate()
quat & Transform::GetRotation()
{
	//Simply return reference to rotation quaternion.
	return m_rotation;
}
