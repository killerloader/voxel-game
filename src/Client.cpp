#include "Client.h"
#include "OtherPlayer.h"
#include "Player.h"

Client::Client()
{
	m_connected = false;
	m_gotID = false;

	//Initialize the Raknet peer interface first 
	m_peerInterface = RakNet::RakPeerInterface::GetInstance();

	for (int i = 0; i < PLAYERCOUNT; i++)
		m_otherPlayers[i] = nullptr;
}

Client::~Client()
{
	//If connected, force disconnection.
	if (m_connected)
	{
		//Shutdown, wait 100ms for message. then close connection (Not sure if both are required)
		m_peerInterface->Shutdown(100);
		m_peerInterface->CloseConnection(m_serverAddr, true);
	}


	RakNet::RakPeerInterface::DestroyInstance(m_peerInterface);
}

void Client::Connect()
{
	//Create a socket descriptor to describe this connection 
	//No data needed, as we will be connecting to a server 
	RakNet::SocketDescriptor sd;

	//Now call startup - max of 1 connections (to the server) 
	m_peerInterface->Startup(1, &sd, 1);
	std::cout << "Connecting to server at: " << IP << std::endl;

	//Now call connect to attempt to connect to the given server 
	RakNet::ConnectionAttemptResult res = m_peerInterface->Connect(IP, PORT, nullptr, 0);

	//Finally, check to see if we connected, and if not, throw a error 
	if (res != RakNet::CONNECTION_ATTEMPT_STARTED)
	{
		std::cout << "Unable to start connection, Error number: " << res << std::endl;
	}
}

void Client::Update(float dt)
{
	HandleNetworkMessages();

	for (int i = 0; i < PLAYERCOUNT; i++)
	{
		if (m_otherPlayers[i] != nullptr)
			m_otherPlayers[i]->Update(dt);
	}
}

void Client::Draw()
{
	for (int i = 0; i < PLAYERCOUNT; i++)
	{
		if (m_otherPlayers[i] != nullptr)
			m_otherPlayers[i]->Draw();
	}
}

void Client::HandleNetworkMessages()
{
	RakNet::Packet* packet;
	for (packet = m_peerInterface->Receive(); packet; m_peerInterface->DeallocatePacket(packet), packet = m_peerInterface->Receive())
	{
		switch (packet->data[0])
		{
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			std::cout << "Another client has disconnected.\n";
			break;
		case ID_REMOTE_CONNECTION_LOST:
			std::cout << "Another client has lost the connection.\n";
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			std::cout << "Another client has connected.\n";
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
			std::cout << "Our connection request has been accepted.\n";
			m_connected = true;
			m_serverAddr = packet->systemAddress;
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			std::cout << "The server is full.\n";
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			std::cout << "We have been disconnected.\n";
			m_connected = false;
			break;
		case ID_CONNECTION_LOST:
			std::cout << "Connection lost.\n";
			m_connected = false;
			break;
		case 140://Get other player data
		{
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(1);//Size of message.
			unsigned char ID; bsIn.Read(ID);

			bool onGround; bsIn.Read(onGround);
			vec3 vel; bsIn.Read(vel);
			vec3 position; bsIn.Read(position);

			if (ID < PLAYERCOUNT && m_otherPlayers[ID] != nullptr)
			{
				//Onground?
				m_otherPlayers[ID]->SetVelocity(vel);
				m_otherPlayers[ID]->GetTransform().SetPosition(position);

				//std::cout << "Moving player: " << (int)ID << "(" << position.x << ", " << position.y << ", " << position.z << ")" << std::endl;
			}
			else
			{
				std::cout << "Non existent player: " << (int)ID << std::endl;
			}
			break;
		}

		case 141://Destroy other player
		{
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(1);//Size of message.
			unsigned char ID; bsIn.Read(ID);

			if (ID < PLAYERCOUNT && m_otherPlayers[ID] != nullptr)
			{
				delete m_otherPlayers[ID];
				m_otherPlayers[ID] = nullptr;
				std::cout << "Destroying other player!" << std::endl;
			}
			else
				std::cout << "other player doesn't exist!" << std::endl;
			break;
		}

		case 136://set ID
		if(m_connected)
		{
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(1);//Size of message.
			unsigned char ID; bsIn.Read(ID);
			if (!m_gotID)
			{
				m_ID = ID;
				m_gotID = true;
				std::cout << "Server has given ID: " << (int)m_ID << std::endl;
			}
			else
			{
				std::cout << "Got another player with ID: " << (int)ID << std::endl;
				m_otherPlayers[ID] = new OtherPlayer;
			}
		}
		break;
		default:
			std::cout << "Received a message with a unknown id: " << packet->data[0];
			break;
		}
	}
}

bool Client::IsConnected()
{
	return m_connected;
}

void Client::SendPlayerData(Player* plr)
{
	if (!IsConnected())
		return;

	RakNet::BitStream bs;
	bs.Write((unsigned char)140);
	bs.Write(m_ID);

	//Upload player data.
	bs.Write(plr->GetOnGround());
	bs.Write(plr->GetVelocity());
	bs.Write(plr->GetTransform().GetPosition());

	m_peerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, m_serverAddr, false);
}

OtherPlayer ** Client::GetOtherPlayers()
{
	return m_otherPlayers;
}
