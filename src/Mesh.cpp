#include "Mesh.h"
#include <vector>
#include <iostream>
#include "ResourceManager.h"

#include "tiny_obj_loader.h"

//Constructor.
Mesh::Mesh()
{
	m_morphing = false;
}

/*
	Mesh deconstructor.
	Only call this if item is not in the resource manager.
*/
Mesh::~Mesh()
{
	glDeleteVertexArrays(1, &m_VAO);
	glDeleteBuffers(1, &m_VBO);

	if(m_morphing)
		glDeleteBuffers(1, &m_VBO2);

	if(m_type == EMeshType_IBO)
		glDeleteBuffers(1, &m_IBO);
}

//Draw the mesh. Doesn't mess with any shaders, just simply draws the vertex array.
void Mesh::Draw()
{
	glBindVertexArray(m_VAO);
	glDrawArrays(GL_TRIANGLES, 0, m_vertexCount);
}

//Generate a cube and add it to the resource manager.
Mesh* Mesh::GenerateCube(std::string meshName, float length, float width, float height, vec2 repeat, vec4 colour)
{
	//Create new mesh.
	Mesh* newMesh = new Mesh();

	Vertex* aoVertices = new Vertex[8];//8 vertices of cube.

	float hw = width / 2.0f;
	float hl = length / 2.0f;
	float hh = height / 2.0f;
	//setup verts.
	aoVertices[0].position = vec4(-hw, hh, -hl, 1);
	aoVertices[1].position = vec4(hw, hh, -hl, 1);
	aoVertices[2].position = vec4(hw, hh, hl, 1);
	aoVertices[3].position = vec4(-hw, hh, hl, 1);
	aoVertices[4].position = vec4(-hw, -hh, -hl, 1);
	aoVertices[5].position = vec4(hw, -hh, -hl, 1);
	aoVertices[6].position = vec4(hw, -hh, hl, 1);
	aoVertices[7].position = vec4(-hw, -hh, hl, 1);

	newMesh->m_vertexCount = 6 * 6;

	// defining index count based off quad count (2 triangles per quad) 
	unsigned int index = 0;

	vec4 V;
	vec4 W;
	vec4 N;
	vec3 tan;

	//Generate buffer, because can't get accurate normals without vertex duplicates.
	Vertex* vertices = new Vertex[36];//6 vertices for each side including duplicates, 6 * 6 sides

	for (unsigned int i = 0; i < 8; ++i)
	{
		aoVertices[i].normal = vec4(0);
		aoVertices[i].color = colour;
	}

	//Define function to save space.
	//Creates a triangle, calculates its normal.
#define addTriangle(s1, s2, s3) \
	vertices[index++] = aoVertices[s1]; vertices[index++] = aoVertices[s2]; vertices[index++]  = aoVertices[s3];\
	V = aoVertices[s2].position - aoVertices[s1].position;\
	W = aoVertices[s3].position - aoVertices[s1].position;\
	N = glm::normalize(vec4(glm::cross(vec3(V.x, V.y, V.z), vec3(W.x, W.y, W.z)), 0));\
	vertices[index - 3].normal = N;\
	vertices[index - 2].normal = N;\
	vertices[index - 1].normal = N;\

	//Define function to save space.
	//Creates the UV for the cube.
	//Also calculates the tangent for both triangles.
#define doUV(startIndex)\
	vertices[startIndex].uv = vec2(0, 0)* repeat; \
	vertices[startIndex + 1].uv = vec2(0, 1) * repeat;\
	vertices[startIndex + 2].uv = vec2(1, 1) * repeat; \
	vertices[startIndex + 3].uv = vec2(1, 1) * repeat; \
	vertices[startIndex + 4].uv = vec2(1, 0) * repeat; \
	vertices[startIndex + 5].uv = vec2(0, 0)* repeat; \
	tan = CalculateTangent(vertices[startIndex], vertices[startIndex + 1], vertices[startIndex + 2]);\
	vertices[startIndex].tangent = vec4(tan, 0);\
	vertices[startIndex + 1].tangent = vec4(tan, 0);\
	vertices[startIndex + 2].tangent = vec4(tan, 0);\
	tan = CalculateTangent(vertices[startIndex+3], vertices[startIndex + 4], vertices[startIndex + 5]); \
	vertices[startIndex + 3].tangent = vec4(tan, 0); \
	vertices[startIndex + 4].tangent = vec4(tan, 0); \
	vertices[startIndex + 5].tangent = vec4(tan, 0);
	//for (int i = 0; i < 6; i++)\
	//	vertices[startIndex + i].tangent = vertices[startIndex + 4].position - vertices[startIndex + 0].position; \
	//Top face
	addTriangle(2, 1, 0);
	addTriangle(0, 3, 2);
	doUV(0)

	//Bottom face
	addTriangle(6, 7, 4)
	addTriangle(4, 5, 6);
	doUV(6)

	//Left face
	addTriangle(1, 5, 4);
	addTriangle(4, 0, 1);
	doUV(12)

	//Right face
	addTriangle(3, 7, 6);
	addTriangle(6, 2, 3);
	doUV(18)

	//Front face
	addTriangle(2, 6, 5);
	addTriangle(5, 1, 2);
	doUV(24)

	//Back face
	addTriangle(0, 4, 7);
	addTriangle(7, 3, 0);
	doUV(30)

	// Generate our GL Buffers 
	// Lets move these so that they are all generated together 
	glGenBuffers(1, &newMesh->m_VBO);
	//Add the following line to generate a VertexArrayObject 
	glGenVertexArrays(1, &newMesh->m_VAO);
	glBindVertexArray(newMesh->m_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, newMesh->m_VBO);
	glBufferData(GL_ARRAY_BUFFER, newMesh->m_vertexCount * sizeof(Vertex), vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4)));
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 2));
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 3));
	glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 4));
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Delete verts.
	delete[] aoVertices;
	delete[] vertices;

	//Add to resource manager. If already exists, delte it.
	if (!ResourceManager::Instance().AddMesh(newMesh, meshName))
	{
		delete newMesh;

		return nullptr;
	}

	return newMesh;
}

//Generate a plane.
Mesh* Mesh::GeneratePlane(std::string meshName, float width, float height, vec2 repeat, vec4 colour)
{
	Mesh* newMesh = new Mesh();
	newMesh->m_vertexCount = 6;

	float hw = width / 2.0f;
	float hh = height / 2.0f;

	//Setup verts
	Vertex vertices[6] = {
		//Position			//Normal		//Tangent		//Colour		//UV
	{	vec4(hw,0,hh,1),	vec4(0,1,0,0),	vec4(1,0,0,0),	colour,	vec2(1) * repeat },
	{	vec4(hw,0,-hh,1),	vec4(0,1,0,0),	vec4(1,0,0,0),	colour,	vec2(1,0) * repeat },
	{	vec4(-hw,0,-hh,1),	vec4(0,1,0,0),	vec4(1,0,0,0),	colour,	vec2(0) * repeat },
	{	vec4(-hw,0,-hh,1),	vec4(0,1,0,0),	vec4(1,0,0,0),	colour,	vec2(0) * repeat },
	{	vec4(-hw,0,hh,1),	vec4(0,1,0,0),	vec4(1,0,0,0),	colour,	vec2(0,1) * repeat },
	{	vec4(hw,0,hh,1),	vec4(0,1,0,0),	vec4(1,0,0,0),	colour,	vec2(1) * repeat }
};

	glGenBuffers(1, &newMesh->m_VBO);
	//Add the following line to generate a VertexArrayObject 
	glGenVertexArrays(1, &newMesh->m_VAO);
	glBindVertexArray(newMesh->m_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, newMesh->m_VBO);
	glBufferData(GL_ARRAY_BUFFER, newMesh->m_vertexCount * sizeof(Vertex), vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4)));
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 2));
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 3));
	glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 4));
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Add mesh to resource manager if it doesn't already exist.
	//Other wise delete it.
	if (!ResourceManager::Instance().AddMesh(newMesh, meshName))
	{
		delete newMesh;

		return nullptr;
	}

	return newMesh;
}

//Generate a fullscreen quad.
Mesh* Mesh::GenerateFullscreenQuad(std::string meshName, int screenWidth, int screenHeight)
{
	Mesh* newMesh = new Mesh();
	newMesh->m_vertexCount = 6;

	vec2 halfTexel = 1.0f / vec2(screenWidth, screenHeight) * 0.5f;

	//Create vertices. Ignore normal, tangent and colour.
	Vertex vertices[6] = {
		//Position			//Normal	//Tangent	//Colour		//UV
		{ vec4(-1,-1,0,1),	vec4(0),	vec4(0),	vec4(0),	vec2(halfTexel.x, halfTexel.y) },
		{ vec4(1,1,0,1),	vec4(0),	vec4(0),	vec4(0),	vec2(1.0f - halfTexel.x,1.0f - halfTexel.y) },
		{ vec4(-1,1,0,1),	vec4(0),	vec4(0),	vec4(0),	vec2(halfTexel.x, 1.0f - halfTexel.y) },
		{ vec4(-1,-1,0,1),	vec4(0),	vec4(0),	vec4(0),	vec2(halfTexel.x, halfTexel.y) },
		{ vec4(1,-1,0,1),	vec4(0),	vec4(0),	vec4(0),	vec2(1.0f - halfTexel.x, halfTexel.y) },
		{ vec4(1,1,0,1),	vec4(0),	vec4(0),	vec4(0),	vec2(1.0f - halfTexel.x, 1.0f - halfTexel.y) }
	};

	glGenBuffers(1, &newMesh->m_VBO);
	//Add the following line to generate a VertexArrayObject 
	glGenVertexArrays(1, &newMesh->m_VAO);
	glBindVertexArray(newMesh->m_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, newMesh->m_VBO);
	glBufferData(GL_ARRAY_BUFFER, newMesh->m_vertexCount * sizeof(Vertex), vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4)));
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 2));
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 3));
	glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 4));
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Add to resource manager if possible. Otherwise delete mesh.
	if (!ResourceManager::Instance().AddMesh(newMesh, meshName))
	{
		delete newMesh;

		return nullptr;
	}

	return newMesh;
}

//Calculate the tangent of a triangle, given three verts.
//This method requires positions and UVs to be set
vec3 Mesh::CalculateTangent(Vertex & vert1, Vertex & vert2, Vertex & vert3)
{
	// Shortcuts for vertices
	glm::vec4 & v0 = vert1.position;
	glm::vec4 & v1 = vert2.position;
	glm::vec4 & v2 = vert3.position;

	// Shortcuts for UVs
	glm::vec2 & uv0 = vert1.uv;
	glm::vec2 & uv1 = vert2.uv;
	glm::vec2 & uv2 = vert3.uv;

	// Edges of the triangle : postion delta
	glm::vec4 deltaPos1 = v1 - v0;
	glm::vec4 deltaPos2 = v2 - v0;

	// UV delta
	glm::vec2 deltaUV1 = uv1 - uv0;
	glm::vec2 deltaUV2 = uv2 - uv0;

	float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
	glm::vec4 tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y)*r;

	return vec3(tangent);
}

//Load and generate a 3D model given an object file
std::vector<Mesh*> Mesh::GenerateModel(std::string meshName, std::string fileName)
{
	//Create an array of meshes.
	std::vector<Mesh*> meshList;

	//Load object with tinObjLoader.
	tinyobj::attrib_t attribs;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;
	bool success = tinyobj::LoadObj(&attribs, &shapes, &materials, &err, fileName.c_str());
	if (!success)//Failed to load, so return empty array, and show error.
	{
		std::cout << err << std::endl;
		return std::vector<Mesh*>();
	}

	//Load all shapes.
	for (unsigned int s = 0; s < shapes.size(); s++)
	{
		Mesh* newModel = new Mesh();

		tinyobj::shape_t& shape = shapes[s];

		// setup OpenGL data 
		glGenVertexArrays(1, &newModel->m_VAO);
		glGenBuffers(1, &newModel->m_VBO);
		glBindVertexArray(newModel->m_VAO);

		newModel->m_vertexCount = shape.mesh.num_face_vertices.size() * 3;

		// collect triangle vertices 
		Vertex* vertices = new Vertex[newModel->m_vertexCount];
		int index = 0;
		for (auto face : shape.mesh.num_face_vertices)
		{
			vec3 pts[3];
			vec2 uvs[3];
			for (int i = 0; i < 3; ++i)
			{
				tinyobj::index_t idx = shape.mesh.indices[index + i];
				Vertex v;
				// positions 
				v.position.x = attribs.vertices[3 * idx.vertex_index + 0];
				v.position.y = attribs.vertices[3 * idx.vertex_index + 1];
				v.position.z = attribs.vertices[3 * idx.vertex_index + 2];
				v.position.w = 1.0f;

				pts[i].x = v.position.x;
				pts[i].y = v.position.y;
				pts[i].z = v.position.z;
				// normals 

				v.normal = vec4(1);
				if (attribs.normals.size() > 0)
				{
					v.normal.x = attribs.normals[3 * idx.normal_index + 0];
					v.normal.y = attribs.normals[3 * idx.normal_index + 1];
					v.normal.z = attribs.normals[3 * idx.normal_index + 2];
					v.normal.w = 1.0f;
				}

				v.tangent = vec4(1, 1, 1, 1);

				// texture coordinates 
				if (attribs.texcoords.size() > 0)
				{
					v.uv.x = attribs.texcoords[2 * idx.texcoord_index + 0];
					v.uv.y = 1.0f - attribs.texcoords[2 * idx.texcoord_index + 1];
				}

				v.color = vec4(1);
				vertices[index + i] = v;
			}

			//Calculate tangent of this face.
			vec3 tan = CalculateTangent(vertices[index], vertices[index + 1], vertices[index + 2]);

			vertices[index].tangent = vec4(tan, 1);
			vertices[index + 1].tangent = vec4(tan, 1);
			vertices[index + 2].tangent = vec4(tan, 1);

			index += face;
		}

		// bind vertex data 
		glBindBuffer(GL_ARRAY_BUFFER, newModel->m_VBO);
		glBufferData(GL_ARRAY_BUFFER, newModel->m_vertexCount * sizeof(Vertex), vertices, GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4)));
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 2));
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 3));
		glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 4));

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		delete[] vertices;

		meshList.push_back(newModel);
	}

	//Add mesh to resource manager if possible. Otherwise delete and return an empty array.
	if (!ResourceManager::Instance().AddMesh(meshList, meshName))
	{
		for (unsigned int i = 0; i < meshList.size(); i++)
			delete meshList[i];

		return std::vector<Mesh*>();
	}

	return meshList;
}

//Generate a morphing animation given two obj files.
std::vector<Mesh*> Mesh::GenerateMorphingAnimationModel(std::string meshName, std::string fileName, std::string fileName2)
{
	//Create an array of meshes.
	std::vector<Mesh*> meshList;

	//Load both obj files and store information.
	tinyobj::attrib_t attribs[2];
	std::vector<tinyobj::shape_t> shapes[2];
	std::vector<tinyobj::material_t> materials[2];
	std::string err[2];
	bool success = tinyobj::LoadObj(&attribs[0], &shapes[0], &materials[0], &err[0], fileName.c_str());
	if (!success)
	{
		std::cout << err << std::endl;
		return std::vector<Mesh*>();
	}

	//Second mode load.
	success = tinyobj::LoadObj(&attribs[1], &shapes[1], &materials[1], &err[1], fileName2.c_str());
	if (!success)
	{
		std::cout << err << std::endl;
		return std::vector<Mesh*>();
	}

	//Load all shapes.
	for (unsigned int s = 0; s < shapes[0].size(); s++)
	{
		//Store model mesh.
		Mesh* newModel = new Mesh();

		//Specify that this mesh a part of a morphing animation.
		newModel->m_morphing = true;

		glGenVertexArrays(1, &newModel->m_VAO);
		glBindVertexArray(newModel->m_VAO);

		//Load the two models.
		for (int m = 0; m < 2; m++)
		{
			unsigned int& usedVBO = (m == 0 ? newModel->m_VBO : newModel->m_VBO2);

			tinyobj::shape_t& shape = shapes[m][s];

			// setup OpenGL data 
			
			glGenBuffers(1, &usedVBO);

			newModel->m_vertexCount = shape.mesh.num_face_vertices.size() * 3;

			// collect triangle vertices 
			Vertex* vertices = new Vertex[newModel->m_vertexCount];
			int index = 0;
			for (auto face : shape.mesh.num_face_vertices)
			{
				vec3 pts[3];
				vec2 uvs[3];
				for (int i = 0; i < 3; ++i)
				{
					tinyobj::index_t idx = shape.mesh.indices[index + i];
					Vertex v;
					// positions 
					v.position.x = attribs[m].vertices[3 * idx.vertex_index + 0];
					v.position.y = attribs[m].vertices[3 * idx.vertex_index + 1];
					v.position.z = attribs[m].vertices[3 * idx.vertex_index + 2];
					v.position.w = 1.0f;

					pts[i].x = v.position.x;
					pts[i].y = v.position.y;
					pts[i].z = v.position.z;
					// normals 

					v.normal = vec4(1);
					if (attribs[m].normals.size() > 0)
					{
						v.normal.x = attribs[m].normals[3 * idx.normal_index + 0];
						v.normal.y = attribs[m].normals[3 * idx.normal_index + 1];
						v.normal.z = attribs[m].normals[3 * idx.normal_index + 2];
						v.normal.w = 1.0f;
					}

					v.tangent = vec4(1, 1, 1, 1);

					// texture coordinates 
					if (attribs[m].texcoords.size() > 0)
					{
						v.uv.x = attribs[m].texcoords[2 * idx.texcoord_index + 0];
						v.uv.y = 1.0f - attribs[m].texcoords[2 * idx.texcoord_index + 1];
					}

					v.color = vec4(1);
					vertices[index + i] = v;
				}

				vec3 tan = CalculateTangent(vertices[index], vertices[index + 1], vertices[index + 2]);

				vertices[index].tangent = vec4(tan, 1);
				vertices[index + 1].tangent = vec4(tan, 1);
				vertices[index + 2].tangent = vec4(tan, 1);

				index += face;
			}

			// bind vertex data 
			glBindBuffer(GL_ARRAY_BUFFER, usedVBO);
			glBufferData(GL_ARRAY_BUFFER, newModel->m_vertexCount * sizeof(Vertex), vertices, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			delete[] vertices;
		}

		glBindBuffer(GL_ARRAY_BUFFER, newModel->m_VBO);

		//Enable all vertex attribute array locations.
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);
		glEnableVertexAttribArray(5);
		glEnableVertexAttribArray(6);
		glEnableVertexAttribArray(7);

		//Set model 1 information.
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);//Position
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 1));//Normal
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 2));//Tangent
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 3));//Colour
		glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 4));//UV

		//Bind the second model VBO
		glBindBuffer(GL_ARRAY_BUFFER, newModel->m_VBO2);

		//Set model 2 information.
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);//Position2
		glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 1));//Normal2
		glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 2));//Tangent2

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);
		meshList.push_back(newModel);
	}
	
	//Add mesh to resource manager if it doesn't already exist.
	//If it already exists, delete it.
	if (!ResourceManager::Instance().AddMesh(meshList, meshName))
	{
		for (unsigned int i = 0; i < meshList.size(); i++)
			delete meshList[i];

		return std::vector<Mesh*>();
	}

	return meshList;
}