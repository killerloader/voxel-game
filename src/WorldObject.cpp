#include "WorldObject.h"
#include "Mesh.h"
#include "Shader.h"

//Constructor.
WorldObject::WorldObject()
{
}

//Destructor.
WorldObject::~WorldObject()
{
}

/*
	Adds a mesh to this object, to be drawn with it.
	All meshes added to an object will be treated the same.
	Each mesh is still separate, so it is not efficient like batching.
*/
void WorldObject::AddMesh(Mesh * mesh)
{
	m_meshes.push_back(mesh);
}

/*
	Adds a list of meshes to this object, to be drawn with it.
	All meshes added to an object will be treated the same.
	Each mesh is still separate, so it is not efficient like batching.
*/
void WorldObject::AddMeshes(std::vector<Mesh*>& meshes)
{
	//Add each individual mesh.
	for (unsigned int i = 0; i < meshes.size(); i++)
		m_meshes.push_back(meshes[i]);
}

//Get a reference material of this object.
//This can be directly edited to change the objects material settings.
Material & WorldObject::GetMaterial()
{
	return m_material;
}

//Draws all the meshes in this object and uses the information from the material to uploaded data to the shader.
void WorldObject::Draw()
{
	//Upload model matrix.
	Shader::GetCurrentProgramShader()->SetMat4("modelMatrix", m_transform.GetWorldTransform());

	//Check which type of shader is currently bound.
	switch (Shader::GetCurrentProgramShader()->GetShaderID())
	{
	case EShaderID_GPass://This is the only one that really matters.
	{
		//Set textures.
		Texture* diffuse = m_material.GetDiffuse();
		Texture* norm = m_material.GetNormalMap();
		Texture* spec = m_material.GetSpecularMap();

		Shader::GetCurrentProgramShader()->SetInt("hasDiffuse", diffuse == nullptr ? 0 : 1);
		if (diffuse != nullptr)
			Shader::GetCurrentProgramShader()->SetTexture("diffuse", diffuse, 0);

		Shader::GetCurrentProgramShader()->SetInt("hasNormalMap", (norm == nullptr || !m_material.GetNormalMapEnabled()) ? 0 : 1);
		if (norm != nullptr && m_material.GetNormalMapEnabled())
			Shader::GetCurrentProgramShader()->SetTexture("normalMap", norm, 1);

		Shader::GetCurrentProgramShader()->SetInt("hasSpecular", spec == nullptr ? 0 : 1);
		if (spec != nullptr)
			Shader::GetCurrentProgramShader()->SetTexture("specular", spec, 2);

		//Set material properties.
		Shader::GetCurrentProgramShader()->SetFloat("metalic", m_material.GetMetalic());
		Shader::GetCurrentProgramShader()->SetFloat("specular", m_material.GetSpecular());
		Shader::GetCurrentProgramShader()->SetFloat("roughness", m_material.GetRoughness());
	}
		break;
	}

	//Draw all meshes in this object.
	for (unsigned int i = 0; i < m_meshes.size(); i++)
		m_meshes[i]->Draw();
}