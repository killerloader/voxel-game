#pragma once
#include <string>
#include "GL/gl3w.h"
#include <GLFW/glfw3.h>

#include "ResourceManager.h"

//texture class.
//Allows loading and storage of textures.
class Texture
{
public:
	int GetTextureID();	//Get the internal texture ID of this texture.

	//Load a texture given a texture filename, texture type and filter type.
	//Also a texture resource name.
	static Texture* LoadTexture(std::string textureName, std::string fName, int TextureType = GL_RGBA, int FilterType = GL_LINEAR);

	//Textures created using this method need to be deleted manually.
	static Texture* ImportTextureID(unsigned int TextureID);

	//Delete this texture.
	//This will only work if this is a "temporary texture"
	//That is a texture that was created with ImportTextureID()
	//These are textures that don't actually "own" their texture ID, and therefore don't delete them.
	void DestroyTemporary();
private:
	unsigned int m_textureID;	//Texture Id of this texture.

	//If this is true, the destructor will destroy this resource, if false, it will now.
	//If the texture is loaded throught ImportTextureID(), then the texture belongs to something else, so it shouldn't be deleted (a framebuffer, or another texture)
	//Need to call DestroyIfTemporary() method to destroy.
	bool m_destroyResource;

	//Deleted or private constructor/ destructor/ copy methods.
	//makes texture non-copyable.
	Texture(const Texture& other) = delete;
	Texture& operator=(const Texture& other) = delete;
	Texture();
	~Texture();

	//Allow resource manager to delete the class as it is a resource.
	friend ResourceManager;
};