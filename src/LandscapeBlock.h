#pragma once

#include <glm/glm.hpp> 
#include <glm/ext.hpp>

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

//A single landscape block. These are actually templates for block types.
struct LandscapeBlock
{
	//Default constructor, for an object that "doesn't exist", such as air.
	LandscapeBlock()
	{
		AllowSeethrough = true;
	}

	//All textures set to the same one.
	LandscapeBlock(vec2 textureID, bool seeThrough = false, vec3 colour1 = vec3(1), vec3 colour2 = vec3(1))
	{
		this->colour1 = colour1;
		this->colour2 = colour2;

		this->topTextureID = textureID;
		this->bottomTextureID = textureID;
		this->leftTextureID = textureID;
		this->rightTextureID = textureID;
		this->frontTextureID = textureID;
		this->backTextureID = textureID;

		AllowSeethrough = seeThrough;
	}

	//Sets up a block to have a texture for the top, one for the sides and one for the bottom.
	LandscapeBlock(vec2 topTextureID, vec2 sideTextureID, vec2 bottomTextureID, bool seeThrough = false, vec3 colour1 = vec3(1), vec3 colour2 = vec3(1))
	{
		this->colour1 = colour1;
		this->colour2 = colour2;

		this->topTextureID = topTextureID;
		this->bottomTextureID = bottomTextureID;
		this->leftTextureID = sideTextureID;
		this->rightTextureID = sideTextureID;
		this->frontTextureID = sideTextureID;
		this->backTextureID = sideTextureID;

		AllowSeethrough = seeThrough;
	}

	//Sets up a block to have one texture for the top, and another texture for every other side.
	LandscapeBlock(vec2 topTextureID, vec2 sideAndBottomTextureID, bool seeThrough = false, vec3 colour1 = vec3(1), vec3 colour2 = vec3(1))
	{
		this->colour1 = colour1;
		this->colour2 = colour2;

		this->topTextureID = topTextureID;
		this->bottomTextureID = sideAndBottomTextureID;
		this->leftTextureID = sideAndBottomTextureID;
		this->rightTextureID = sideAndBottomTextureID;
		this->frontTextureID = sideAndBottomTextureID;
		this->backTextureID = sideAndBottomTextureID;

		AllowSeethrough = seeThrough;
	}

	vec3 GenerateColour(float percentage = 0.0f)
	{
		return colour1 * percentage + colour2 * (1.0f - percentage);
	}

	//Colours to lerp between
	vec3 colour1;		
	vec3 colour2;

	//If blocks should be rendered throguht this (leaves)
	bool AllowSeethrough;

	//Texture coords for each sides of the block.
	vec2 topTextureID;
	vec2 bottomTextureID;
	vec2 leftTextureID;
	vec2 rightTextureID;
	vec2 frontTextureID;
	vec2 backTextureID;
};