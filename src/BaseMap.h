#pragma once

class Shader;

class BaseMap
{
public:
	BaseMap();
	virtual ~BaseMap();

	//Non deferred rendering.
	virtual void Draw();
	virtual void DrawUI();
	virtual void Update(float dt) = 0;

	bool UsesDeferred();
	bool UsesPostProcessing();

protected:
	bool m_useDeferred;
	bool m_usePostProcessing;
};