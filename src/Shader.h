#pragma once
#include <string>
#include "ResourceManager.h"
#include <map>
#include "GL/gl3w.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 
#include <glm/ext.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;


/*
	Used to identify different shaders when an object could have the possbility of being drawn with different shaders bound.
	There are a lot more shaders than ID's in this list, however they are specific shaders that could not be confused with others.
*/
enum EShaderID
{
	EShaderID_Basic,
	EShaderID_GPass,
	EShaderID_ShadowGen
};

class Texture;

//Shader class.
//Used for creating and binding shader programs.
class Shader
{
public:
	//Create a shader and add it to the resource manager.
	static Shader* CreateShader(std::string shaderName, EShaderID shaderID, std::string vertexFileName, std::string fragmentFileName);

	//Create a shader and add it to the resource manager. Ignore shaderID, it will just be set to basic.
	static Shader* CreateShader(std::string shaderName, std::string vertexFileName, std::string fragmentFileName);

	bool IsProgramBound();	//Check if this program is bound.
	void Bind();			//Bind this program.
	static void UnbindAny();//Unbind any currently bound program.

	bool operator==(const Shader other);		//Compare shader to another shader. Compares their program ID.
	bool operator==(const EShaderID otherID);	//Compare one shader to another useing their shader ID.

	int GetProgramID();		//Get the program ID
	EShaderID GetShaderID();//Get the program shader ID (more genertic way of checking what the shader is)

	//Setters for variables types.

	void SetTexture(std::string variableName, Texture* texture, int slotID);

	//Set int value. Make sure program is bound beforehand.
	void SetInt(std::string variableName, int value);

	//Set float value. Make sure program is bound beforehand.
	void SetFloat(std::string variableName, float value);

	//Set vec4 value. Make sure program is bound beforehand.
	void SetVec4(std::string variableName, const vec4& value);

	//Set vec3 value. Make sure program is bound beforehand.
	void SetVec3(std::string variableName, const vec3& value);

	//Set vec2 value. Make sure program is bound beforehand.
	void SetVec2(std::string variableName, const vec2& value);

	//Set mat4 value. Make sure program is bound beforehand.
	void SetMat4(std::string variableName, const mat4& value);

	//Finds or adds a variable.
	int FindVariable(std::string variableName);

	static std::string LoadFromFile(std::string fileName);	//Load a file and store it in a string. Used to load a shader program.
	static Shader* GetCurrentProgramShader();				//Get a pointer to the currently bound program.
	static bool IsAnyProgramBound();						//Check if any program is bound
private:
	unsigned int m_programID;	//The program ID of this shader.
	EShaderID m_shaderID;	//Helps to identify shader.
	std::map<std::string, int> m_programVariables;	//Variables in shader. Int so it can be -1, which allows non-existent variables to not be processed multiple times.

	static int m_currentBoundProgramID;			//ID of current bound program.
	static Shader* m_currentBoundProgram;		//Pointer to current bound program.
	static void BindProgramID(int programID);	//Method to simply bind a program ID.

	//Delete/ private members, so that the class cannot be copied, created or destroyed from the outside.
	Shader();
	~Shader();
	Shader(const Shader& other) = delete;
	Shader& operator=(const Shader& other) = delete;

	//Allow resource manager to destroy shaders.
	friend ResourceManager;
};