#include "BaseCamera.h"
#include <iostream>

//Base camera constructor.
BaseCamera::BaseCamera()
{
	m_view = mat4(1);
	m_projection = mat4(1);
}

//Deconstructor.
BaseCamera::~BaseCamera()
{
}

//Set the perspective of this camera.
void BaseCamera::SetPerspective(float fov, float apsect, float nearr, float farr)
{
	m_projection = glm::perspective(fov, apsect, nearr, farr);
}

//This method gets the world projection view, and updates the view if the transform is dirty.
//This could cause problems if the transform is updated for some reason before this is called, then the view would not be updated.
//This can be fixed by calling MakeDirty on the transform after doing so.
mat4 BaseCamera::WorldProjectionView()
{
	//Check if the view needs updating.
	if (m_transform.IsDirty())
	{
		//Update view with inverse of world transform.
		m_view = glm::inverse(m_transform.GetWorldTransform());
	}

	return m_projection * m_view;
}

//Get the projection matrix of this camera.
mat4 BaseCamera::Projection()
{
	return m_projection;
}

vec3 BaseCamera::PointToViewSpace(vec3 point)
{
	vec4 clipPos = m_projection * (m_view * vec4(point.x, point.y, point.z, 1));
	vec3 ndcSpacePos = vec3(clipPos.x, clipPos.y, clipPos.z) / clipPos.w;

	return ndcSpacePos;
}

bool BaseCamera::PointInFrustrum(vec3 point)
{
	vec3 ndcSpacePos = PointToViewSpace(point);

	return (
		ndcSpacePos.x >= -1.0f && ndcSpacePos.x <= 1.0f &&
		ndcSpacePos.y >= -1.0f && ndcSpacePos.y <= 1.0f
		);
}

class plane
{
public:
	plane(float a, float b, float c, float d)
	{
		this->a = a;
		this->b = b;
		this->c = c;
		this->b = d;
	}

	plane()
	{
		a = 0.0f;
		b = 0.0f;
		c = 0.0f;
		d = 0.0f;
	}

	float a, b, c, d;
};

bool BaseCamera::BoxIntersectFrustrum(vec3 minPt, vec3 maxPt)
{
	//Extract planes from frustum
	plane l, r, b, t, n, f;

	float mat[16];

	glm::mat4 matt = WorldProjectionView();

	//Custom projection to see culling effect
	//Check if the view needs updating.
	//if (m_transform.IsDirty())
	//{
	//	//Update view with inverse of world transform.
	//	m_view = glm::inverse(m_transform.GetWorldTransform());
	//}
	//
	//glm::mat4 persp = glm::perspective(glm::pi<float>() * 0.1f, 16 / 9.f, 0.1f, 1000.f);
	//
	//matt = persp * m_view;

	for (int i = 0; i < 4; i++)
		for (int ii = 0; ii < 4; ii++)
			mat[i * 4 + ii] = matt[i][ii];

	// Left Plane
	// col4 + col1
	l.a = mat[3] + mat[0];
	l.b = mat[7] + mat[4];
	l.c = mat[11] + mat[8];
	l.d = mat[15] + mat[12];

	// Right Plane
	// col4 - col1
	r.a = mat[3] - mat[0];
	r.b = mat[7] - mat[4];
	r.c = mat[11] - mat[8];
	r.d = mat[15] - mat[12];

	// Bottom Plane
	// col4 + col2
	b.a = mat[3] + mat[1];
	b.b = mat[7] + mat[5];
	b.c = mat[11] + mat[9];
	b.d = mat[15] + mat[13];

	// Top Plane
	// col4 - col2
	t.a = mat[3] - mat[1];
	t.b = mat[7] - mat[5];
	t.c = mat[11] - mat[9];
	t.d = mat[15] - mat[13];

	// Near Plane
	// col4 + col3
	n.a = mat[3] + mat[2];
	n.b = mat[7] + mat[6];
	n.c = mat[11] + mat[10];
	n.d = mat[15] + mat[14];

	// Far Plane
	// col4 - col3
	f.a = mat[3] - mat[2];
	f.b = mat[7] - mat[6];
	f.c = mat[11] - mat[10];
	f.d = mat[15] - mat[14];

	// Indexed for the 'index trick' later
	vec3 box[] = { minPt, maxPt };

	plane* planes[6] =
	{
		&n, &l, &r, &b, &t, &f
	};

	// We only need to do 6 point-plane tests
	for (int i = 0; i < 6; ++i)
	{
		// This is the current plane
		const plane &p = *planes[i];

		// p-vertex selection (with the index trick)
		// According to the plane normal we can know the
		// indices of the positive vertex
		const int px = static_cast<int>(p.a > 0.0f);
		const int py = static_cast<int>(p.b > 0.0f);
		const int pz = static_cast<int>(p.c > 0.0f);

		// Dot product
		// project p-vertex on plane normal
		// (How far is p-vertex from the origin)
		const float dp =
			(p.a*box[px].x) +
			(p.b*box[py].y) +
			(p.c*box[pz].z);

		// Doesn't intersect if it is behind the plane
		if (dp < -p.d) { return false; }
	}

	return true;
}