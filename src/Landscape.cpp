#include "Landscape.h"
#include "LandscapeChunk.h"
#include "Shader.h"
#include "BoxCollider.h"
#include "CollisionRay.h"
#include "Texture.h"
#include "LandscapeBlock.h"
#include <iostream>
#include <thread>
#include <mutex>
#include <atomic>

//Constructor. Setup chunks.
Landscape::Landscape()
{
	//Sets up texture coords for each block type.
	SetupBlocks();

	m_chunks = nullptr;
}

//Destroy landscape.
Landscape::~Landscape()
{
	LandscapeChunk::DestroyThreads();

	for (int i = 0; i < CHUNKS; i++)
	{
		for (int ii = 0; ii < CHUNKS; ii++)
			delete m_chunks[i][ii];

		delete[] m_chunks[i];
	}

	delete[] m_chunks;

	delete[] m_blockTypes;
}

//Setup block templates.
void Landscape::SetupBlocks()
{
	m_blockTypes = new LandscapeBlock[8]{
		//Air
		LandscapeBlock(),
		//Grass
		LandscapeBlock(vec2(0,0), false, vec3(0.3,0.7,0.3),vec3(0.3,0.7,0.3) * 1.1f),
		//Stone
		LandscapeBlock(vec2(1,0), false, vec3(0.5,0.5,0.5), vec3(0.5,0.5,0.5) * 1.2f),
		//Snow
		LandscapeBlock(vec2(2,4), false, vec3(1,1,1), vec3(1,1,1) * 0.96f),
		//Tree 1
		LandscapeBlock(vec2(5,1), false, vec3(0.7,0.4,0), vec3(0.7,0.4,0) * 0.95f),
		//Leaves 1
		LandscapeBlock(vec2(4,3), false, vec3(0.2,0.5,0.2), vec3(0.2,0.5,0.2) * 0.9f),
		//Dirt
		LandscapeBlock(vec2(5,1), false, vec3(0.5,0.4,0.2), vec3(0.5,0.4,0.2) * 0.95f),
		//sand
		LandscapeBlock(vec2(5,1), false, vec3(0.8,0.8,0.6), vec3(0.8,0.8,0.6) * 0.95f),
	};

	return;
	m_blockTypes = new LandscapeBlock[6]{
		//Air
		LandscapeBlock(),
		//Grass
		LandscapeBlock(vec2(0,0), vec2(3,0), vec2(2,0), false, vec3(0.4f,1.0f,0.4f)),
		//Stone
		LandscapeBlock(vec2(1,0)),
		//Snow
		LandscapeBlock(vec2(2,4)),
		//Tree 1
		LandscapeBlock(vec2(5,1), vec2(4,1), vec2(5,1)),
		//Leaves 1
		LandscapeBlock(vec2(4,3), true, vec3(0.3,1,0.3))
	};
}

//Check if a block exists in global space.
bool Landscape::CheckBlock(int x, int y, int z)
{
	int chunkX = (int)glm::floor((float)x / BLOCKS);
	int chunkZ = (int)glm::floor((float)z / BLOCKS);

	int relativeX = x - chunkX * BLOCKS;
	int relativeZ = z - chunkZ * BLOCKS;

	LandscapeChunk* ptr = GlobalChunkToLocalChunk(chunkX, chunkZ);

	if (ptr == nullptr)
		return nullptr;

	return ptr->CheckBlock(relativeX, y, relativeZ);
}

//Get a block in global space.
//Thread safe.
unsigned char Landscape::GetBlock(int x, int y, int z)
{
	int chunkX = (int)glm::floor((float)x / BLOCKS);
	int chunkZ = (int)glm::floor((float)z / BLOCKS);

	int relativeX = x - chunkX * BLOCKS;
	int relativeZ = z - chunkZ * BLOCKS;

	std::lock_guard<std::mutex> lock(m_chunkMutex);

	LandscapeChunk* ptr = GlobalChunkToLocalChunk(chunkX, chunkZ);

	if (ptr == nullptr)
		return 0;

	return ptr->GetBlockType(relativeX, y, relativeZ);
}

//Destroy a block in global space.
void Landscape::DestroyBlock(int x, int y, int z)
{
	int chunkX = (int)glm::floor((float)x / BLOCKS);
	int chunkZ = (int)glm::floor((float)z / BLOCKS);

	int relativeX = x - chunkX * BLOCKS;
	int relativeZ = z - chunkZ * BLOCKS;

	LandscapeChunk* ptro = GlobalChunkToLocalChunk(chunkX, chunkZ);

	if (ptro == nullptr)
		return;

	if (ptro->DestroyBlock(relativeX, y, relativeZ))
	{//Only update surrounding chunks if the block was successfully destroyed.
	 //Make sure connecting chunks are regenerated.

		int yChunkID = y / YBlocksPerChunk;

		LandscapeChunk* ptr = GlobalChunkToLocalChunk(chunkX - 1, chunkZ);

		if (relativeX == 0 && ptr != nullptr)//Left
			ptr->DestroyVBOSingle(yChunkID);

		ptr = GlobalChunkToLocalChunk(chunkX + 1, chunkZ);

		if (relativeX == BLOCKS - 1 && ptr != nullptr)//Right
			ptr->DestroyVBOSingle(yChunkID);

		ptr = GlobalChunkToLocalChunk(chunkX, chunkZ -1);

		if (relativeZ == 0 && ptr != nullptr)//Down
			ptr->DestroyVBOSingle(yChunkID);

		ptr = GlobalChunkToLocalChunk(chunkX, chunkZ + 1);

		if (relativeZ == BLOCKS - 1 && ptr != nullptr)//Up
			ptr->DestroyVBOSingle(yChunkID);

		//Also do it within the chunk.
		//Update y subchunks.
		if (yChunkID != (y + 1) / YBlocksPerChunk)//up
			ptro->DestroyVBOSingle(yChunkID + 1);

		if (yChunkID != (y - 1) / YBlocksPerChunk)//Down
			ptro->DestroyVBOSingle(yChunkID - 1);
	}
}

//Create a block in global space, given a block type.
void Landscape::CreateBlock(int x, int y, int z, unsigned char blockType)
{
	int chunkX = (int)glm::floor((float)x / BLOCKS);
	int chunkZ = (int)glm::floor((float)z / BLOCKS);

	int relativeX = x - chunkX * BLOCKS;
	int relativeZ = z - chunkZ * BLOCKS;

	LandscapeChunk* ptro = GlobalChunkToLocalChunk(chunkX, chunkZ);

	if (ptro == nullptr)
		return;

	if (ptro->CreateBlock(relativeX, y, relativeZ, blockType))
	{//Only update surrounding chunks if the block was successfully destroyed.
	 //Make sure connecting chunks are regenerated.

		int yChunkID = y / YBlocksPerChunk;

		LandscapeChunk* ptr = GlobalChunkToLocalChunk(chunkX - 1, chunkZ);

		if (relativeX == 0 && ptr != nullptr)//Left
			ptr->DestroyVBOSingle(yChunkID);

		ptr = GlobalChunkToLocalChunk(chunkX + 1, chunkZ);

		if (relativeX == BLOCKS - 1 && ptr != nullptr)//Right
			ptr->DestroyVBOSingle(yChunkID);

		ptr = GlobalChunkToLocalChunk(chunkX, chunkZ - 1);

		if (relativeZ == 0 && ptr != nullptr)//Down
			ptr->DestroyVBOSingle(yChunkID);

		ptr = GlobalChunkToLocalChunk(chunkX, chunkZ + 1);

		if (relativeZ == BLOCKS - 1 && ptr != nullptr)//Up
			ptr->DestroyVBOSingle(yChunkID);

		//Also do it within the chunk.
		//Update y subchunks.
		if (yChunkID != (y + 1) / YBlocksPerChunk)//up
			ptro->DestroyVBOSingle(yChunkID + 1);

		if (yChunkID != (y - 1) / YBlocksPerChunk)//Down
			ptro->DestroyVBOSingle(yChunkID - 1);
	}
}

LandscapeChunk * Landscape::GlobalSpaceToLocalChunk(int x, int z)
{
	int chunkX = m_cCenterX + RENDER_DIST + (x / 16);
	int chunkY = m_cCenterY + RENDER_DIST + (z / 16);

	if (chunkX < 0 || chunkY < 0 || chunkX >= CHUNKS || chunkY >= CHUNKS)
		return nullptr;

	return m_chunks[chunkX][chunkY];
}

LandscapeChunk * Landscape::GlobalChunkToLocalChunk(int x, int z)
{
	int chunkX = RENDER_DIST + x - m_cCenterX;
	int chunkY = RENDER_DIST + z - m_cCenterY;

	if (chunkX < 0 || chunkY < 0 || chunkX >= CHUNKS || chunkY >= CHUNKS)
		return nullptr;

	return m_chunks[chunkX][chunkY];
}

LandscapeChunk* Landscape::GetChunk(int X, int Y)
{
	return GlobalChunkToLocalChunk(X, Y);
}

//Get a block type given a block ID.
LandscapeBlock & Landscape::GetBlockType(int ID)
{
	return m_blockTypes[ID];
}

std::mutex & Landscape::GetChunkMutex()
{
	return m_chunkMutex;
}

void Landscape::SetCentralPoint(int sX, int sY, bool force)
{
	if (m_chunks == nullptr)
	{
		m_cCenterX = sX;
		m_cCenterY = sY;

		//Create chunks. (2D array of pointers)
		m_chunks = new LandscapeChunk**[CHUNKS];

		for (int i = 0; i < CHUNKS; i++)
		{
			m_chunks[i] = new LandscapeChunk*[CHUNKS];

			//If not using threading.
			for (int ii = 0; ii < CHUNKS; ii++)
			{
				m_chunks[i][ii] = new LandscapeChunk(m_cCenterX - RENDER_DIST + i, m_cCenterY - RENDER_DIST + ii, this);
			}
		}
	}
	else//Move center
	{
		//Don't move if same.
		if (m_cCenterX == sX && m_cCenterY == sY)
			return;

		//Having this block the program could cause quite a lot of lag.
		if (force)
			m_chunkMutex.lock();
		else if (!m_chunkMutex.try_lock())
			return;

		int cDX = sX - m_cCenterX;
		int cDY = sY - m_cCenterY;

		//Create new chunks.
		LandscapeChunk*** newChunks = new LandscapeChunk**[CHUNKS];

		for (int i = 0; i < CHUNKS; i++)
		{
			newChunks[i] = new LandscapeChunk*[CHUNKS];

			//If not using threading.
			for (int ii = 0; ii < CHUNKS; ii++)
			{
				//Copy over old chunks.

				int oldX = i + cDX;
				int oldY = ii + cDY;

				//Move old chunk
				if (oldX >= 0 && oldY >= 0 && oldX < CHUNKS && oldY < CHUNKS)
					newChunks[i][ii] = m_chunks[oldX][oldY];
				else//create new chunk
					newChunks[i][ii] = new LandscapeChunk(sX - RENDER_DIST + i, sY - RENDER_DIST + ii, this);
			}
		}

		//Deleting old chunks will crash the game due to the other threads trying to read from deleted chunks.

		//make sure to delete old chunks which went nowhere.
		for (int i = 0; i < CHUNKS; i++)
		{
			//If not using threading.
			for (int ii = 0; ii < CHUNKS; ii++)
			{
				//Find where this chunk would go
				int newX = i - cDX;
				int newY = ii - cDY;
		
				//Out of bounds?
				if (newX < 0 || newY < 0 || newX >= CHUNKS || newY >= CHUNKS)
					if (!m_chunks[i][ii]->TryDelete())//Desrtuctor can cause race condition with chunkMutex, so use tryDelete to avoid.
					{
						//Failed! add to queue and delete later!
						m_trashChunks.push_back(m_chunks[i][ii]);
					}
			}
		}

		m_cCenterX = sX;
		m_cCenterY = sY;

		//Delete old arrays
		for (int i = 0; i < CHUNKS; i++)
			delete[] m_chunks[i];

		delete[] m_chunks;

		//Copy to main chunks.
		m_chunks = newChunks;

		m_chunkMutex.unlock();
	}
}

//Draw all chunks in this landscape.
void Landscape::Draw(PlayerCamera* camera)
{
	auto iter = m_trashChunks.begin();

	//Go through trash and see what can be removed.
	while (iter != m_trashChunks.end())
	{
		auto oldIter = iter;
		iter++;

		//Try to delete, erase if successful.
		if ((*oldIter)->TryDelete())
			m_trashChunks.erase(oldIter);
	}

	m_chunks[RENDER_DIST][RENDER_DIST]->Draw(camera);
	//return;

	for (int i = 1; i <= RENDER_DIST; i++)
	{
		int X1 = RENDER_DIST + i;
		int X2 = RENDER_DIST - i;

		int Y1 = RENDER_DIST + i;
		int Y2 = RENDER_DIST - i;

		m_chunks[RENDER_DIST][RENDER_DIST]->Draw(camera);
		
		for (int x = X2; x <= X1; x += (X1 - X2))
			for (int y = Y2; y <= Y1; y++)
				if (x >= 0 && x < CHUNKS && y >= 0 && y < CHUNKS)
					m_chunks[x][y]->Draw(camera);

		for (int x = Y2; x <= Y1; x += (Y1 - Y2))
			for (int y = X2; y <= X1; y++)
				if (x >= 0 && x < CHUNKS && y >= 0 && y < CHUNKS)
					m_chunks[y][x]->Draw(camera);
	}
}

//Check for a collision given a collider. Will return an overlap if poitner specified.
bool Landscape::CheckCollision(BoxCollider & collider, vec3* overlap)
{
	//Calculate intersecting bounds.

	Bounds bnds = collider.GetBounds();
	bnds.min += vec3(0.5f, 0.5f, 0.5f);
	bnds.max += vec3(0.5f, 0.5f, 0.5f);

	int chunkLowX = (int)glm::floor(bnds.min.x / BLOCKS);
	int chunkLowZ = (int)glm::floor(bnds.min.z / BLOCKS);
	int chunkHighX = (int)glm::floor(bnds.max.x / BLOCKS);
	int chunkHighZ = (int)glm::floor(bnds.max.z / BLOCKS);

	bool any = false;

	//Check intersecting chunks.
	for(int i = chunkLowX; i <= chunkHighX; i++)
		for (int ii = chunkLowZ; ii <= chunkHighZ; ii++)
		{
			vec3 tempOverlap;

			LandscapeChunk* chnk = GetChunk(i, ii);

			if (chnk == nullptr)
				continue;

			bool current = chnk->CheckCollision(collider, &tempOverlap);
			any = any || current;

			if (current)
			{
				if (overlap != nullptr)
				{
					//No real overlappable block was found in chunk, so continue to next chunk.
					if (tempOverlap == vec3(0))
						continue;

					*overlap += tempOverlap;
				}

				return true;//Don't bother checking anything else if one is found and no overlap is wanted.
			}
		}

	return any;
}

bool Landscape::CheckCollision(CollisionRay & ray, float length, vec3* intersection, vec3* blockLocation)
{
	vec3 rayPos = ray.GetPosition();

	//Store current position. Start at initial position.
	int cX = glm::floor(rayPos.x + 0.5f);
	int cY = glm::floor(rayPos.y + 0.5f);
	int cZ = glm::floor(rayPos.z + 0.5f);

	//Cache direction of ray so we don't have to obtain it again.
	vec3 dir = ray.GetDirection();

	//Cache signed dirs, so we only check blocks in the direction the ray is heading.
	int xDir = glm::sign(dir.x);
	int yDir = glm::sign(dir.y);
	int zDir = glm::sign(dir.z);

	//Create box collider which can be moved around.
	BoxCollider box(vec3(1));

	//Initially set intersection point to the ray position.
	//If there is an intersection in the first block, then this will be correct.
	*intersection = ray.GetPosition();

	//Continue through collisions until true or out of bounds.
	while (true)
	{
		if (glm::length(*intersection - rayPos) > length)
			break;

		//Check if this block is already set, and if so, success
		if (CheckBlock(cX, cY, cZ))
		{
			*blockLocation = vec3(cX, cY, cZ);
			return true;
		}
			

		//x axis
		if (xDir != 0)
		{
			box.SetTranslation(vec3(cX + xDir, cY, cZ));
			if (ray.CheckAgainst(box, length, intersection))
			{
				cX += xDir;
				continue;
			}
		}

		//y axis
		if (yDir != 0)
		{
			box.SetTranslation(vec3(cX, cY + yDir, cZ));
			if (ray.CheckAgainst(box, length, intersection))
			{
				cY += yDir;
				continue;
			}
		}

		//z axis
		if (zDir != 0)
		{
			box.SetTranslation(vec3(cX, cY, cZ + zDir));
			if (ray.CheckAgainst(box, length, intersection))
			{
				cZ += zDir;
				continue;
			}
		}

		break;
	}

	return false;
}

//Check for a collision given a collider, and an offset. Will return an offset if overlap pointer given.
bool Landscape::CheckCollisionOffset(BoxCollider & collider, vec3 offset, vec3* overlap)
{
	//Translate with offset.
	collider.Translate(offset);

	//Test collision with new position (store result)
	bool collision = CheckCollision(collider, overlap);

	//Translate back to original position.
	collider.Translate(-offset);

	return collision;
}
