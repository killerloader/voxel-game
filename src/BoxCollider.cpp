#include "BoxCollider.h"
#include <iostream>

//Constructor. Sets up initial bounds.
BoxCollider::BoxCollider(vec3 size)
{
	m_bounds.max = size / 2.0f;
	m_bounds.min = -(size / 2.0f);

	m_transformedBounds = m_bounds;
}

BoxCollider::~BoxCollider()
{
}

//Transform the bounds based on a matrix.
//This will reset the bounds to their initial position before transforming.
void BoxCollider::SetTransformation(const mat4 & matrix)
{
	m_transformedBounds.max = vec3(vec4(m_bounds.max, 1) * matrix);
	m_transformedBounds.min = vec3(vec4(m_bounds.min, 1) * matrix);
}

//Transform the bounds based on a translation.
//This will reset the bounds to their initial position before translating.
void BoxCollider::SetTranslation(vec3 & translation)
{
	m_transformedBounds.max = m_bounds.max + translation;
	m_transformedBounds.min = m_bounds.min + translation;
}

//Simply translate the already transformed bounds.
//Doesn't reset the transformation.
void BoxCollider::Translate(vec3 & translate)
{
	m_transformedBounds.max += translate;
	m_transformedBounds.min += translate;
}

//Check for collision against another box collider.
bool BoxCollider::CheckAgainst(BoxCollider & other, vec3* overlap)
{
#define Omax other.m_transformedBounds.max
#define Omin other.m_transformedBounds.min
#define Tmax m_transformedBounds.max
#define Tmin m_transformedBounds.min

	if ((Tmin.x < Omax.x && Tmax.x > Omin.x) &&
	(Tmin.y < Omax.y && Tmax.y > Omin.y) &&
	(Tmin.z < Omax.z && Tmax.z > Omin.z))
	{
		//Find minimum overlap if it exists
		if (overlap != nullptr)
		{
			//Make initial overlap the left overlap.
			float overlapAmt = Omax.x - Tmin.x;
			vec3 direction(-1,0,0);

			//Create define to save space. Checks if new overlap test is smaller than smallest so far.
			#define overlapTest(DIR, OVERLAP)\
			newOverlap = OVERLAP;\
			if (newOverlap < overlapAmt)\
			{\
				overlapAmt = newOverlap;\
				direction = DIR;\
			}

			float newOverlap;

			//Find minimum overlap.
			overlapTest(vec3(1, 0, 0), Tmax.x - Omin.x);
			overlapTest(vec3(0, -1, 0), Omax.y - Tmin.y);
			overlapTest(vec3(0, 1, 0), Tmax.y - Omin.y);
			overlapTest(vec3(0, 0, -1), Omax.z - Tmin.z);
			overlapTest(vec3(0, 0, 1), Tmax.z - Omin.z);

			//Find the minimum translation vector.
			vec3 overlapTotal = direction * overlapAmt;

			*overlap = overlapTotal;
		} 

		//There was collision.
		return true;
	}

	//There was no collision.
	return false;
}

//Return the transformed bounds.
Bounds BoxCollider::GetBounds() const
{
	return m_transformedBounds;
}
