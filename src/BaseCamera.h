#pragma once
#include "GL/gl3w.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 
#include <glm/ext.hpp>

#include "WorldData.h"

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

//Base camera template class. Provides general data for cameras.
//Also derives from worlddata, so it contains a transform.
class BaseCamera : public WorldData
{
public:
	BaseCamera();	//Constructor.
	~BaseCamera();	//Destructor.
	virtual void Update(GLFWwindow* window, float dt) = 0;	//Pure virtual camera update method.
	void SetPerspective(float fov, float apsect, float nearr, float farr);	//Set perspective method.
	mat4 WorldProjectionView();	//Method to get the world projection view.
	mat4 Projection();			//Method to get the projection of the matrix.
	
	vec3 PointToViewSpace(vec3 point);
	bool PointInFrustrum(vec3 point);
	bool BoxIntersectFrustrum(vec3 minPt, vec3 maxPt);
	
private:
	mat4 m_view;		//Internal view matrix.
	mat4 m_projection;	//Internal projection matrix.
};

