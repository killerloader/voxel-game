#include "ResourceManager.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include <iostream>

ResourceManager* ResourceManager::m_instance = nullptr;

//Singleton instance access.
ResourceManager & ResourceManager::Instance()
{
	if (m_instance == nullptr)
		m_instance = new ResourceManager();

	return *m_instance;
}

//Destroy the singleton.
void ResourceManager::DestroyInstance()
{
	delete m_instance;
	m_instance = nullptr;
}

//Add a single mesh to the resource manager.
bool ResourceManager::AddMesh(Mesh * mesh, std::string name)
{
	//Check if mesh already exists.
	if (FindMesh(name) != nullptr)
	{
		std::cout << "Mesh with name: '" << name << "' already exists!" << std::endl;
		return false;
	}

	ItemGroup<std::vector<Mesh*>> newMesh;
	newMesh.m_name = name;
	newMesh.m_data.push_back(mesh);
	m_meshes.push_back(newMesh);

	return true;
}

//Add a vector of meshes to the resource manager.
bool ResourceManager::AddMesh(const std::vector<Mesh*> meshes, std::string name)
{
	//Check if mesh group already exists.
	if (FindMeshes(name) != std::vector<Mesh*>())
	{
		std::cout << "Mesh group with name: '" << name << "' already exists!" << std::endl;
		return false;
	}

	ItemGroup<std::vector<Mesh*>> newMesh;
	newMesh.m_name = name;
	newMesh.m_data = meshes;
	m_meshes.push_back(newMesh);

	return true;
}

//Find a vector of meshes in the resource manager.
std::vector<Mesh *> ResourceManager::FindMeshes(std::string name)
{
	for (unsigned int i = 0; i < m_meshes.size(); i++)
	{
		if (m_meshes[i].m_name == name)
			return m_meshes[i].m_data;
	}

	return std::vector<Mesh*>();
}

//Find a single mesh in the resource manager.
Mesh* ResourceManager::FindMesh(std::string name)
{
	for (unsigned int i = 0; i < m_meshes.size(); i++)
	{
		if (m_meshes[i].m_name == name && m_meshes[i].m_data.size() != 0)
			return m_meshes[i].m_data[0];
	}

	return nullptr;
}

//Add a texture to the resource manager.
bool ResourceManager::AddTexture(Texture * texture, std::string name)
{
	//Check if texture already exists.
	if (FindTexture(name) != nullptr)
	{
		std::cout << "Texture with name: '" << name << "' already exists!" << std::endl;
		return false;
	}

	//Cannot add temporary textures to the manager.
	if (!texture->m_destroyResource)
		return false;

	ItemGroup<Texture*> newTexture;
	newTexture.m_name = name;
	newTexture.m_data = texture;
	m_textures.push_back(newTexture);

	return true;
}

//Find a single texture from the resource manager
Texture * ResourceManager::FindTexture(std::string name)
{
	for (unsigned int i = 0; i < m_textures.size(); i++)
	{
		if (m_textures[i].m_name == name)
			return m_textures[i].m_data;
	}

	return nullptr;
}

//Add a single shader to the resource manager.
bool ResourceManager::AddShader(Shader * shader, std::string name)
{
	//Check if shader already exists.
	if (FindShader(name) != nullptr)
	{
		std::cout << "Shader with name: '" << name << "' already exists!" << std::endl;
		return false;
	}

	ItemGroup<Shader*> newShader;
	newShader.m_name = name;
	newShader.m_data = shader;
	m_shaders.push_back(newShader);

	return true;
}

//Find a single shader from the resource manager.
Shader * ResourceManager::FindShader(std::string name)
{
	for (unsigned int i = 0; i < m_shaders.size(); i++)
	{
		if (m_shaders[i].m_name == name)
			return m_shaders[i].m_data;
	}

	return nullptr;
}

//Constructor.
ResourceManager::ResourceManager()
{
}

//Destructor. Destroy all data.
ResourceManager::~ResourceManager()
{
	//Loop through data and destroy everything.
	for (unsigned int i = 0; i < m_shaders.size(); i++)
		delete m_shaders[i].m_data;

	for (unsigned int i = 0; i < m_textures.size(); i++)
		delete m_textures[i].m_data;

	for (unsigned int i = 0; i < m_meshes.size(); i++)
		for (unsigned int ii = 0; ii < m_meshes[i].m_data.size(); ii++)//Some meshes have multiple mehses, so destroy them all.
			delete m_meshes[i].m_data[ii];
}