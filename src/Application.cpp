#include "Application.h"
#include "Texture.h"
#include "Shader.h"
#include "WorldObject.h"
#include "MinecraftMap.h"
#include "Mesh.h"
#include <iostream>

//Internal singleton instance.
Application* Application::m_instance = nullptr;

//Singleton global access point.
Application & Application::Instance()
{
	//Make new instance if none exist.
	if (m_instance == nullptr)
		m_instance = new Application();

	//Return reference to instance.
	return *m_instance;
}

void Application::Destroy()
{
	if (m_instance != nullptr)
		delete m_instance;
	m_instance = nullptr;
}

double Application::timeAtPt = 0.0;

void Application::StartTimeRecord()
{
	timeAtPt = glfwGetTime();
}

void Application::EndTimeRecord(std::string name, double msThreshold)
{
	double timeDiff = glfwGetTime() - timeAtPt;
	double timeMS = timeDiff * 1000.0;

	if(timeMS >= msThreshold)
		std::cout << name << ": " << timeMS <<  "ms" << std::endl;
}

//Simple error callback function for glfw.
static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error %d: %s\n", error, description);
}

//Application constructor
//This can only be created via the global access point.
Application::Application()
{
	m_roomChanged = false;

	//Set error callback.
	glfwSetErrorCallback(error_callback);

	//Attempt to initialize glfw. If fail, exit
	if (glfwInit() == false)
		return;

	m_window = nullptr;

	bool fullscreen = false;
	bool vsync = true;

	//Attempt to create window, if fail, exit.
	m_window = glfwCreateWindow(1920, 1080, "Computer Graphics", fullscreen ? glfwGetPrimaryMonitor() : nullptr, nullptr);

	glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

	bool anyNull = false;

	anyNull = m_window == nullptr;

	//Create worker contexts for each thread.
	for (int i = 0; i < THREAD_COUNT; i++)
	{
		m_threadContexts[i] = glfwCreateWindow(1920, 1080, "", nullptr, m_window);
		m_threadContextUsed[i] = false;

		anyNull = anyNull || m_threadContexts[i] == nullptr;
	}

	if (anyNull) { glfwTerminate(); return; }

	//Make the new window the current context.
	glfwMakeContextCurrent(m_window);

	//Initialize gl3w for opengl functions.
	gl3wInit();

	//Enable depth testing.
	glEnable(GL_DEPTH_TEST);

	//Enable depth masking.
	glDepthMask(GL_TRUE);

	//Enable back face culling.
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// Setup ImGui binding
	ImGui_ImplGlfwGL3_Init(m_window, true);

	//vsync, 60fps lock.
	glfwSwapInterval(vsync ? 1 : 0);

	//Get display framebuffer.
	int display_w, display_h;
	glfwGetFramebufferSize(m_window, &display_w, &display_h);

	CreateResources();

	//Create the current map, start the menu map as the first map.
	m_currentMap = nullptr;
	ChangeRoom(new MinecraftMap());

	m_mousePosition = vec2(0);
}

Application::~Application()
{
	//Destroy resource manager.
	ResourceManager::DestroyInstance();

	//Delete map.
	delete m_currentMap;

	//Shutdown glfw.
	ImGui_ImplGlfwGL3_Shutdown();
	glfwTerminate();
}

//Run the game loop.
void Application::Run() 
{
	//Don't try to run if window never opened.
	if (m_window == nullptr)
		return;

	//Get current display size.
	int display_w, display_h;
	glfwGetFramebufferSize(m_window, &display_w, &display_h);

	//Run game loop.
	while (glfwWindowShouldClose(m_window) == false)
	{
		glfwPollEvents();
		ImGui_ImplGlfwGL3_NewFrame();

		GLenum err = glGetError();

		if (err != GL_NO_ERROR)
		{
			std::string aError;

			switch (err)
			{
			case GL_INVALID_ENUM: aError = "GL_INVALID_ENUM"; break;
			case GL_INVALID_VALUE: aError = "GL_INVALID_VALUE"; break;
			case GL_INVALID_OPERATION: aError = "GL_INVALID_OPERATION"; break;
			case GL_INVALID_FRAMEBUFFER_OPERATION: aError = "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
			case GL_OUT_OF_MEMORY: aError = "GL_OUT_OF_MEMORY"; break;
			case GL_STACK_UNDERFLOW: aError = "GL_STACK_UNDERFLOW"; break;
			case GL_STACK_OVERFLOW: aError = "GL_STACK_OVERFLOW"; break;
			}

			std::cout << err << ": " << aError << std::endl;
		}

		//Get mouse positions.
		double mX, mY;
		glfwGetCursorPos(m_window, &mX, &mY);
		m_mousePosition.x = (float)mX;
		m_mousePosition.y = (float)mY;

		Update(0.01f);

		if (m_roomChanged)
		{
			m_roomChanged = false;
			continue;
		}

		//Update viewport.
	 	glfwGetFramebufferSize(m_window, &display_w, &display_h);
		glViewport(0, 0, display_w, display_h);

		Draw();

		glfwSwapBuffers(m_window);
	}
}

//Main draw method.
void Application::Draw()
{
	m_currentMap->Draw();

	DrawUI();
}

//Draw UI method.
//Can either use ImGui, or you can draw fullscreen quads with UI on them.
void Application::DrawUI()
{
	//Call current map draw UI method.
	m_currentMap->DrawUI();
}

//Updates the current map.
void Application::Update(float dt)
{
	//Call current map update method.
	m_currentMap->Update(dt);
}

void Application::CreateResources()
{
	//MC
	Shader::CreateShader("McShader", "Data\\Shaders\\vs.shader", "Data\\Shaders\\fs.shader");
	Shader::CreateShader("McShaderNormal", "Data\\Shaders\\vsn.shader", "Data\\Shaders\\fsn.shader");
	Mesh::GenerateCube("PlayerCube", 1.0f, 1.0f, 1.5f, vec2(1), vec4(0.75f, 0.75f, 1, 1));

	Mesh::GenerateCube("SelectCube", 1.0f, 1.0f, 1.0f, vec2(1), vec4(0.4f, 0.4f, 1, 0.3f));
	Mesh::GenerateCube("FaceCube", 0.25f, 0.25f, 0.25f, vec2(1), vec4(1, 0.4f, 0.4f, 0.5f));
}

//Get window method.
//Returns a point to the main window.
GLFWwindow * Application::GetWindow()
{
	return m_window;
}

//Get window method.
//Returns a pointer to the shared window context.
GLFWwindow * Application::GetFreeWorkerContext(int& ID)
{
	std::lock_guard<std::mutex> guard(contextMutex);

	//If we got to he, we successfully locked it! :D

	for (int i = 0; i < THREAD_COUNT; i++)
	{
		if (!m_threadContextUsed[i])
		{
			m_threadContextUsed[i] = true;
			ID = i;
			return m_threadContexts[i];
		}
	}

	return nullptr;
}

void Application::FreeWorkerContext(int ID)
{
	std::lock_guard<std::mutex> guard(contextMutex);

	//If we got to he, we successfully locked it! :D

	m_threadContextUsed[ID] = false;
}

//Get mouse position method.
//Returns mouse position.
vec2 Application::GetMousePosition()
{
	return m_mousePosition;
}

//Change room method.
//Deletes the current room and changes to the new room.
//As the old room is deleted straight away, and the old room most likely called this method, it is unsafe to
//continue running code from that class, therefore it should quit straight away.
void Application::ChangeRoom(BaseMap * newRoom)
{
	//Delete current map if it exists.
	if(m_currentMap != nullptr)
		delete m_currentMap;

	//Set current map to new map.
	m_currentMap = newRoom;

	m_roomChanged = true;
}

//Gets the framebuffer size (pretty much the screen size)
//Uses slow glfw method.
vec2 Application::GetFramebufferSize()
{
	int w, h;
	glfwGetFramebufferSize(m_window, &w, &h);
	return vec2(w,h);
}

bool Application::IsWindowFocused()
{
	return glfwGetWindowAttrib(m_window, GLFW_FOCUSED);
}
