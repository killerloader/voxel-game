#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#define TINYOBJLOADER_IMPLEMENTATION//Only needs to be in one
#include "other\tiny_obj_loader.h"
#define STB_IMAGE_IMPLEMENTATION//Only needs to be in one
#include "other\stb_image.h"
#include "Application.h"

/*
TODO/ Notes
	- Commenting.
	- Post processing techniques.
	- Delete everything.
	- Change minecraft textures to custom textures.
	- Remove landscape inst variable, give access some other way.
	- Try to fix soulspear model?
	- Generate spheres.
	- Generate bounds when creating meshes.
	- Actually calcualt deltatime...

	Maybe:
	- Possibly a resource manager that holds all shaders/ meshes/ textures
	As well as that, these classes should not be able to be copied, so they can be more easily destroyed.
	- Deferred shading/ lighting (two render passes for material properties)
	Or alternatively (and slowly) just upload required information to another buffer. (specular)
	- PBR lighting, if the previous thing is done.
	- Second part of deferred rendering for point lights.
	- Shadows should work the opposite, and check for non-shadow places. This way, every single texel wont have to check for shadow, and it probably makes more sense.
	As every texel can first check if it is in shadow before checking if there is any non-shadow around it.
	It makes more sense for light to bleed into the shadow like this, instead of the shadow bleeding into the lit areas.

	Minecraft:
	- 2ND method for disconnection where disconnect packet gets sent from client to disconnect more quickly.
	- Send block changes to client.
	- Create VBOs on server.
	- Instead of contracting out of ground (which seems to produce random bugs such as infinite loops and teleportation), when we check if we 
	can actually be pushed in a certain direction by a block, allow the push if the player is already overlapping with the block in that direction.
	This means that if you're already inside a block, you wont collide with it, and you can jump out if you want.
	Combine this with the current method, so it only works like this after attempting to push the player out.
	- Give each chunk 16 vertical chunks (VBOS), though each chunk will only have one VBO linked to 16 VBOs, so there shouldn't be any extra draw calls, and drawing the VBOs wont cost extra.
	When a VBO changes, it shouldn't need to be updated in the VAO either.

*/

//Program entry point.
void main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	//Rand at the start once.
	srand((unsigned int)time(0));

	//Run the application singleton.
	Application::Instance().Run();
	Application::Destroy();

	return;
}