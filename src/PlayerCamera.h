#pragma once
#include "BaseCamera.h"

class Transform;

//Player camera.
//A camera that follows a transform and acts like a first person camera.
class PlayerCamera : public BaseCamera
{
public:
	PlayerCamera();	//Constructor.
	~PlayerCamera();//Destructor.

	void Update(GLFWwindow* window, float dt);	//Update method. Controls movement/ rotation etc.
	void UpdateMousePos(vec2 pos);			//Gives the class access to the mouse position.
	void SetTarget(Transform* transform);	//Sets the camera's transform target. Essentialy following that target.

private:
	Transform* m_playerTransform;	//Pointer to players transform (or targets transform)
	vec2 m_mousePos;				//Mouse position.
	vec2 m_oldMousePos;				//Old mouse position (for finding movement speed)
	vec2 m_storedMove;				//Stored mouse movement so we can recreate the camera rotation every frame.
	vec2 m_actualMove;				//Stored mouse movement so we can recreate the camera rotation every frame.
	bool firstMouseEvent;			//If this is the first mouse movement, so we don't think the mouse moved from 0,0 to wherever it is.
};