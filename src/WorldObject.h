#pragma once
#include "WorldData.h"
#include "Material.h"
#include <vector>

class Texture;
class Mesh;

/*
World object class.
Simply stores a mesh (or a list of meshes), a transform, and material information.

Passes own information to current shader by checking which shader is currently linked.
*/
class WorldObject : public WorldData
{
public:
	WorldObject();	//Constructor.
	~WorldObject();	//Destructor.

	void AddMesh(Mesh* mesh);	//Adds a mesh to this object.
	void AddMeshes(std::vector<Mesh*>& meshes);	//Adds a list of meshes to this object.

	Material& GetMaterial();	//Return reference to material object.

	virtual void Draw();	//Draw the object.

private:
	Material m_material;
	std::vector<Mesh*> m_meshes;	//List of meshes this object uses.
};