#pragma once

#include <glm/glm.hpp> 
#include <glm/ext.hpp>
#include <glm/gtc/noise.hpp>
#include "WorldData.h"

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

class BoxCollider;

class CollisionRay
{
public:
	CollisionRay(vec3 pos, vec3 dir);
	~CollisionRay();

	bool CheckAgainst(const BoxCollider& boxCollider, float length = 100.0f, vec3* intersection = nullptr);

	vec3 GetPosition();
	vec3 GetDirection();

private:
	vec3 m_position;
	vec3 m_direction;
};