#pragma once
#include "Texture.h"

//Material class.
//Holds all information about the material of something.
//Can be used to pull values out and place into a shader.
class Material
{
public:
	Material();	//Constructor. Sets default values.
	~Material();//Destructor.

	void SetNormalMap(Texture* texture);	//Sets the normal map texture.
	void SetDiffuse(Texture* texture);		//Sets the diffuse texture.
	void SetSpecularMap(Texture* texture);	//Sets the specular map texture.
	void SetRoughness(float roughness);		//Sets the material roughness.
	void SetSpecular(float specular);		//Sets the material specular.
	void SetMetalic(float metalic);			//Sets the material metalic.

	void SetNormalMapEnabled(bool enabled);	//Sets whether the normal map should be used or not (for toggling)
	bool GetNormalMapEnabled();	//Gets whether the normal map should be used or not.

	Texture* GetDiffuse();		//Get the diffuse texture.
	Texture* GetNormalMap();	//Get the normal map.
	Texture* GetSpecularMap();	//Get the specular map.
	float GetRoughness();		//Get the material roughness.
	float GetSpecular();		//Get the material specular.
	float GetMetalic();			//Get the materal metalic.

private:
	Texture* m_normalMapTexture;	//normal map texture
	Texture* m_diffuseTexture;		//diffuse texture.
	Texture* m_specularMapTexture;	//specular texture.
	float m_specular;	//specular component.
	float m_roughness;	//roughness component.
	float m_metalic;	//metalic component.

	bool m_normlMapEnabled;	//If the normal map is enabled or not.
};

