#pragma once

//IMGUI stuff
#include <imgui.h>
#include "imgui_impl_glfw_gl3.h"

#include "GL/gl3w.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 
#include <glm/ext.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <thread>
#include <mutex>

//Thread count and also worker contexts so each thread can have one.
#define THREAD_COUNT 3

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

class FlyCamera;
class BaseMap;
class PostFrameBuffer;
class GPassFrameBuffer;
class LightFrameBuffer;
class ShadowFrameBuffer;
class Mesh;
class Shader;

/*
	Main application class, everything runs from here.
*/
class Application
{
public:
	void Run();	//Run the game loop.

	GLFWwindow* GetWindow();	//Returns window pointer.
	GLFWwindow* GetFreeWorkerContext(int& ID);	//Returns window pointer.
	void FreeWorkerContext(int ID);
	vec2 GetMousePosition();	//Returns mouse position.
	void ChangeRoom(BaseMap* newRoom);	//Changes the current map, and destroys the old one.
	vec2 GetFramebufferSize();		//Returns the backbuffer size.

	bool IsWindowFocused();

	static Application& Instance();	//Instance to singleton.
	static void Destroy();	//Destroy singleton.

	static void StartTimeRecord();
	static void EndTimeRecord(std::string name, double msThreshold = 0.0);

	static double timeAtPt;

private:
	//Singleton Instance;
	static Application* m_instance;

	//Private/ deleted implicit constructors/ copy methods. (For singleton)
	Application();	//private constructor (singleton)
	~Application();	//private deconstrcutor.
	Application(const Application& o) = delete;				//Deleted copy constructor.
	Application& operator=(const Application& o) = delete;	//Deleted copy assignment operator.

	GLFWwindow* m_window;			//Window pointer.
	GLFWwindow* m_threadContexts[THREAD_COUNT];	//shared window pointer.
	bool m_threadContextUsed[THREAD_COUNT];

	std::mutex contextMutex;

	BaseMap* m_currentMap;			//Pointer to current map.

	vec2 m_mousePosition;	//The current mouse position.

	void Draw();			//Basic draw method.
	void DrawUI();			//UI draw method.
	void Update(float dt);	//Update method.

	void CreateResources();

	bool m_roomChanged = false;
};