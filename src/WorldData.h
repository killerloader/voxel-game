#pragma once
#include "Transform.h"

/*
	An abstract class with information for world space.
	Anything that exists in the world should use this abstract class.
*/
class WorldData
{
public:
	Transform& GetTransform();	//Get transform method, so transform is accessible from outside.
	virtual ~WorldData() = 0 {/* Empty Implementation */};	//Pure virtual destructor, so cannot be instantiated.

protected:
	Transform m_transform;	//Internal transform. Protected so classes can easily access.
};