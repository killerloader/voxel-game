#pragma once
#include "GL/gl3w.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 
#include <glm/ext.hpp>
#include <glm/gtc/noise.hpp>
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>
#include <stack>
#include "Application.h"
#include "FastNoise.h"

/*
	TODO:
		- VAO and VBO generated should never really be set to false until the chunk is completely deleted, as there will alwas be something in VAO if it has been generated once
		This is because the worker thread only creates a new VAO in a separate m_VAO variable (m_VAONew), then the main thread switches them when it's ready.
		- Lock block access in thread when generating grid (only really at start of game)
		The main thread can just return false (or return an air block) if it is currently locked.
		- Need some way for the thread to tell the main thread.
*/

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

struct LandVertex
{
	vec4 position;
	vec4 normal;
	vec4 color;
	vec4 AOcolor;

	void SetUV(vec2 uv)
	{
		position.w = uv.x;
		normal.w = uv.y;
	}
};

class Shader;
class BoxCollider;
class Landscape;

//Parameters for minecraft chunk.
#define BLOCKS 16
#define BLOCKSY 256

#define AOADDITIVE false
#define AOVALUE 0.4f

//2 = fade over whole block, less than 2 = less fade.
#define LINEARGI 0.25f

//Unused (Wasn't working in shader?)
//#define EXPONENTIALGI 1.0f

//Virtual Y chunks for each chunk, to split up regeneration.
#define YCHUNKS 16
#define YBlocksPerChunk 16

enum Biomes
{
	Biome_Grass,
	Biome_Snow,
	Biome_Desert
};

class PlayerCamera;

struct BlockInstance
{
	unsigned char blockType;
	float colourPercentage;

	void GenerateColourPercentage()
	{
		colourPercentage = glm::linearRand(0.0f, 1.0f);
	}
};

//A single chunk in a minecraft world.
class LandscapeChunk
{
public:
	LandscapeChunk(int chunkX, int chunkZ, Landscape* landscapePtr);	//Constructor. Setup chunk.
	~LandscapeChunk();	//Deconstructor for chunk. Destroy VBO.

	void GenerateGrid();	//Generate the grid for collision and for the VBO later on.
	void CreateTree(int x, int y, int z);	//Generate a tree at a position.
	void GenerateVBO();	//Generate VBO for this so it can be drawn.
	void GenerateVAO();	//Generate VBO for this so it can be drawn.
	void GenerateVBOSingle(int YChunkID);
	void DestroyVBOSingle(int YChunkID, bool delet = false);	//Destroy the VBO of this chunk.
	void DestroyVBO(bool delet = false);	//Destroy all VBOs
	void GenerateVBOAsync();

	static Biomes Biome(vec2 pos);

	bool TryDelete();

	static void DestroyThreads();

	int GetGlobalBlock();

	unsigned char GetBlockType(int x, int y, int z);	//Get the block type at a position (0 = air) (relative to chunk)
	float GetBlockPercentage(int x, int y, int z);
	bool CheckBlock(int x, int y, int z);	//Check if a block exists at a grid location. (relative to chunk)
	bool DestroyBlock(int x, int y, int z);	//Destroy a block at a grid location. (relative to chunk)
	bool CreateBlock(int x, int y, int z, unsigned char blockType);	//Create a block at a grid location.
	bool CheckCollision(BoxCollider & collider, vec3* overlap = nullptr);	//Check for a collision given a collider.

	static void AddToPriorityQueue(LandscapeChunk* chunk);
	static bool PriorityQueueIsEmpty();

	void Draw(PlayerCamera* camera);	//Draw the chunk.

private:
	BlockInstance*** m_grid;	//The grid of blocks. Simply stored as a single byte each.
	std::atomic<bool> m_VBOGenerated[YCHUNKS];
	std::atomic<bool> m_VAOGenerated[YCHUNKS];
	unsigned int m_VBO[YCHUNKS];	//Chunk VBO
	unsigned int m_VBOOld[YCHUNKS];	//Chunk VBO
	unsigned int m_VAO[YCHUNKS];	//chunk VAO

	bool m_VBOneedsRegeneration[YCHUNKS];
	bool m_VBOBeingRegenerated[YCHUNKS];

	//
	std::mutex newObjectMutex;
	//For thread, so it doesn't touch the VAO/ VBO of the main thread.
	//Don't need VAO, as that is done on main thread
	unsigned int m_VBONew[YCHUNKS];	//Chunk Old VBO
	
	bool chunkUpdateAvailable;

	//unsigned int m_indexCount;	//Index count
	unsigned int m_vertexCount[YCHUNKS];	//Vertex count
	unsigned int m_vertexCountNew[YCHUNKS];	//Vertex count
	int m_chunkX, m_chunkZ;	//The chunk ID of this chunk.
	std::atomic<bool> m_gridGenerated;	//If the block data has been generated.

	static std::atomic<bool> m_threadActive[THREAD_COUNT];

	std::mutex chunkThreadMutex;
	std::mutex chunkMutex;
	
	static std::thread* chunkWorker[THREAD_COUNT];
	static LandscapeChunk* curGeneration[THREAD_COUNT];
	static std::atomic<bool> chunkWorkerShutdown;
	std::atomic<bool> threadActiveOnSelf;

	static FastNoise noise;

	int m_workerID;

	static LandVertex* m_workerVertArrays[THREAD_COUNT];

	static int FindThread();

	static std::stack<LandscapeChunk*> m_priorityChunks;
	bool m_inPriorityQueue;

	std::mutex test;

	Landscape* m_landscapePtr;
};